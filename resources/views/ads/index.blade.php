@extends('layouts.app')
@php $locale = session()->get('locale'); @endphp
@section('content')
    <div class="h-20"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                @include('widgets.sidebar')
            </div>
            <div class="col-md-9">

                <div class="content-page-title">
                    <i class="fa fa-empire"></i> {{__('ads.ads')}}
                    <a href="{{url('ads/all')}}" class="btn  float-right">{{'Show all'}}</a>
                </div>
                <div class="content-page-title">
                   <a href="{{url('ads/create')}}" class="btn btn-primary">{{__('ads.create_ad')}}</a>
                </div>
                @foreach($ads_categories as $ads_category)
                <div class="content-page-blue-title no-padding-bottom">
                    <a href="{{url('ads_category/')}}" class="btn  "><h3>{{$ads_category->name}}</h3></a>
                </div>
                @if($ads->count() == 0)
                    <div class="alert-message alert-message-default">
                        <h4>{{__('ads.noads')}}</h4>
                    </div>
                @else
                    <div class="row" >
                        @foreach($ads as $jey => $ad)
                            @if($ad->category_id==$ads_category->id)
                            <div class="col-sm-6 col-md-4">
                                <div class="detail">
                                    <div class="image ">
                                        @if($ad->image=='')
                                        <a data-fancybox="group"  href="{{url('storage/6063934.png') }}">
                                            <img class="img-responsive " src="{{ url('storage/6063934.png') }}" alt="" style="height: 260px" />
                                        </a>
                                        @else
                                        <a data-fancybox="group"  href="{{url('storage/uploads/ads/'.$ad->image) }}">
                                            <img class="img-responsive " src="{{ url('storage/uploads/ads/'.$ad->image) }}" alt="" style="height: 260px" />
                                        </a>
                                        @endif
                                    </div>

                                    <span class="{{($locale =='ar')?'float-left':'float-right'}} padding5">{{round($ad->price, 1)}}$</span>
                                    <a class="bs-box " style="max-height: 90px" href="{{url('ad/'.$ad->id)}}">
                                        <h4>{{ $ad->title }}</h4>
                                        @if($ad->state !='')
                                        <p style="color:#325f85"><i class="fa fa-map-marker"></i> {{' '.$ad->state}}</p>
                                        @endif
                                    </a>
                                </div>
                            </div>
                            @endif
                        @endforeach
                    </div>
                @endif
                @endforeach
            </div>
        </div>
    </div>
@endsection

@section('footer')

@endsection