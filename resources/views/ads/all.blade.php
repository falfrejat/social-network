@extends('layouts.app')
@php $locale = session()->get('locale'); @endphp
@section('content')
    <div class="h-20"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                @include('widgets.sidebar')
            </div>
            <div class="col-md-9">

                <div class="content-page-title">
                    <i class="fa fa-empire"></i> {{__('ads.ads')}}
                    <a href="{{url('ads/all')}}" class="btn  float-right">{{'Show all'}}</a>
                </div>
{{--                <div class="content-page-title">--}}
{{--                   <a href="{{url('ads/create')}}" class="btn btn-primary">{{__('ads.create_ad')}}</a>--}}
{{--                </div>--}}

                @if($ads->count() == 0)
                    <div class="alert-message alert-message-default">
                        <h4>{{__('ads.noads')}}</h4>
                    </div>
                @else
                    <div class="row" >
                        @foreach($ads as $jey => $ad)

                            <div class="col-sm-6 col-md-4">
                                <div class="detail">
                                    <div class="image ">
                                        <a data-fancybox="group"  href="{{url('storage/uploads/ads/_n1.jpg') }}">
                                            <img class="img-responsive " src="{{ url('storage/uploads/ads/_n1.jpg') }}" alt="" />
                                        </a>
                                    </div>

                                    <span class="{{($locale =='ar')?'float-left':'float-right'}} padding5">{{$ad->price}}$</span>
                                    <a class="bs-box " style="max-height: 90px" href="{{url('ad/'.$ad->id)}}">
                                        <h4>{{ $ad->title }}</h4>
                                    </a>
                                </div>
                            </div>

                        @endforeach
                    </div>
                @endif

            </div>
        </div>
    </div>
@endsection

@section('footer')

@endsection