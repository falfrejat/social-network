@extends('layouts.app')

@section('content')
    <div class="h-20"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                @include('widgets.sidebar')
            </div>

            <div class="col-md-9">
                @if(Session::has('alert-success'))
                    <div class="alert alert-success"><i class="fa fa-check" aria-hidden="true"></i> <strong>{!! session('alert-success') !!}</strong></div>
                @endif
                @if(Session::has('alert-danger'))
                    <div class="alert alert-danger"><i class="fa fa-times" aria-hidden="true"></i> <strong>{!! session('alert-danger') !!}</strong></div>
                @endif
            </div>
            <div class="col-md-9">

                <div class="content-page-title">
                    <i class="fa fa-users"></i> {{__('ads.ads')}}
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">{{__('ads.create_ad')}}</div>

                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('ads/create') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">{{__('ads.title')}}</label>
                                <div class="col-md-6">
                                    <input id="title" type="text" class="form-control" name="title">
                                    @if ($errors->has('title'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('title') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="category" class="col-md-4 control-label">{{__('ads.category')}}</label>
                                <div class="col-md-6">
                                    <select class="form-control " id="category" name="category" style="border-left: 1px solid #ccc;">
                                        @foreach($ads_categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('category'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('category') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="price" class="col-md-4 control-label">{{__('ads.price')}}</label>
                                <div class="col-md-6">
                                    <input id="price" type="number" class="form-control" name="price" placeholder="$">
                                    @if ($errors->has('price'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('price') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="country" class="col-md-4 control-label">{{__('home.country')}}</label>
                                <div class="col-md-6">
                                    <select class="form-control countries" name="country" id="countryId">
                                        <option value="0">{{__('home.chooseCountry')}}</option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="state" class="col-md-4 control-label">{{__('home.city')}}</label>
                                <div class="col-md-6">
                                    <select class="form-control states" name="state" id="stateId">

                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">{{__('ads.content')}}</label>

                                <div class="col-md-6">
                                    <textarea id="text" type="text" class="form-control" name="content"></textarea>

                                    @if ($errors->has('content'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('content') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="formFileLg" class="col-md-4 control-label">{{__('ads.image')}}</label>
                                <div class="col-md-6">
                                 <input class="form-control form-control-lg" name="image" id="formFileLg" type="file" value="" />
                                    @if ($errors->has('image'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('image') }}</strong>
                                        </span>
                                    @endif
                                 </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{__('groups.create')}}
                                    </button>
                                    <a href="{{url('/')}}" class="btn btn-default">
                                        {{__('home.cancel')}}
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('countryfooter')
    <script src="{{ asset('js/countrystatecity.js')}}"></script>
@endpush
