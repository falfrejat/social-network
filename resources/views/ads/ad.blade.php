@extends('layouts.app')

@section('content')
    <div class="h-20"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                @include('widgets.sidebar')
            </div>
            <div class="col-md-9">
                <div class="content-page-title">
                    <i class="fa fa-"></i> {{$ad->title}}
                </div>
                <div class="row">
                    <ul class="list-group">
                        @if (!empty($ad->image))
                        <li class="list-group-item content" >
                            <img width="100%" src="{{url('storage/uploads/ads/'.$ad->image)}}">
                        </li>
                        @endif
                        <li class="list-group-item">
                           <div class="col-lg-6"><b>{{__('jobs.title')}}</b></div>
                            <div class="col-lg-6">{{$ad->title}}</div>
                        </li>
                        <li class="list-group-item">
                            <div class="col-lg-6"><b>{{__('ads.price')}}</b></div>
                            <div class="col-lg-6">{{$ad->price}}</div>
                        </li>
                        <li class="list-group-item">
                            <div class="col-lg-6"><b>{{__('ads.category')}}</b></div>
                            <div class="col-lg-6">{{($ad->category)?$ad->category->name:''}}</div>
                        </li>

                        <li class="list-group-item content" >
                            <div class="col-lg-6"><b>{{__('jobs.details')}}</b></div>
                            <div class="col-lg-6">{{$ad->text}}</div>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
    <style>
        .list-group-item{
            height: 40px;
        }
        .content{
            height: auto;
            display: block;
            overflow: auto;
        }
    </style>

@endsection

@section('footer')

@endsection