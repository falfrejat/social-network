@extends('layouts.app')

@section('header')


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/css/bootstrap-select.css">


@endsection
@section('content')
    <div class="h-20"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                @include('widgets.sidebar')
            </div>
            <div class="col-md-9">

                <div class="content-page-title">
                    {{ $group->hobby->name }}
                    <div class="col-md-4 pull-right">
{{--                        <a href="{{url('groups/invite')}}" class="btn btn-info">{{__('groups.invite')}}</a>--}}
                        <div class="button-frame">
                            <a href="javascript:;" data-toggle="modal" data-target="#groupInvite">
                                <i class="fa fa-plus-circle"></i>
                                {{__('groups.invite')}}
                            </a>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-xs-12 col-md-3 pull-right">
                        <div class="hidden-sm hidden-xs">
                            @include('pages.widgets.count')
                            @include('pages.widgets.people_in')
                        </div>
                    </div>
                    <div class="col-md-9">
                        <!-- need edit -->
                        @include('widgets.wall')
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script type="text/javascript">
        WALL_ACTIVE = true;
        fetchPost(2,0,{{ $group->id }},10,-1,-1,'initialize');
    </script>
    <script src="{{ asset('js/group.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/js/bootstrap-select.js"></script>
    <script>
        $('select').selectpicker();
    </script>
@endsection
<div class="modal fade" id="groupInvite" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title">{{__('groups.invite_others')}}</h5>
            </div>

            <div class="modal-body">
                <form id="form-groupInvite">
                    <!--add Location -->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12 map-place"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    @if($list->count() == 0)
                        <div class="alert-message alert-message-danger">
                            <h4>Following are not found.</h4>
                        </div>
                    @else
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>{{__('home.following')}}</label>

                                <select class="selectpicker form-control" multiple data-live-search="true" id="invited" name="invited">
                                    @foreach($list as $relation)
                                    <option value="{{ $relation->following->id }}">{{ $relation->following->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    @endif
                </form>
            </div>

            <div class="modal-footer">
                @if($list->count() != 0)
                <button type="button" class="btn btn-success" onclick="invite()">{{__('groups.invite')}}</button>
                @endif
                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('home.close')}}</button>
            </div>
        </div>
    </div>
</div>