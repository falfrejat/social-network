

<div class="profile-information">
    @if($my_profile)
        <div class="edit-button">
            <div class="button-frame">
                <a href="javascript:;" data-toggle="modal" data-target="#profileInformation">
                    <i class="fa fa-pencil"></i>
                    {{__('home.edit')}}
                </a>
            </div>
        </div>
    @endif
    <ul class="list-group">
        <li class="list-group-item">
            @if($user->sex == 0)
                <i class="fa fa-mars"></i>
            @else
                <i class="fa fa-venus"></i>
            @endif
            {{ $user->getSex() }}
        </li>

        @if($user->has('location'))
        <li class="list-group-item">
            <i class="fa fa-map-marker"></i>
            {{ $user->location->city->name }}
        </li>
        @endif
        @if ($user->phone)
        <li class="list-group-item">
            <i class="fa fa-mobile"></i>
            {{ $user->getPhone() }}
        </li>
        @endif
        @if ($user->birthday)
        <li class="list-group-item">
            <i class="fa fa-birthday-cake"></i>
            {{ $user->birthday->format('d.m.Y') }} - {{ $user->getAge() }}
        </li>
        @endif
        @if ($user->bio)
        <li class="list-group-item">
            <i class="fa fa-info-circle"></i>
            {{ $user->bio }}
        </li>
        @endif
    </ul>
</div>

<!-- work experience -->
@if($user->workExperience()->get() !=[])
<div class="panel panel-default">
   <div class="panel-heading">{{__('jobs.work_experience')}} @if($my_profile) <a href="javascript:;" data-toggle="modal" data-target="#work_experience"><i>{{ __('home.edit') }}</i></a> @endif</div>
    @forelse($user->workExperience()->get() as $x)
   <ul class="list-group" style="max-height: 300px; overflow-x: auto">
       @if($x->workexpereince != '')
       <li class="list-group-item">{{__('jobs.last_work').' : '.$x->workexpereince }}</li>
       @endif
       @if($x->cv != '')
       <li class="list-group-item">{{__('jobs.your_cv').' : '}}<a target="_blank" href="{{url('workexp/'.$x->id.'/downloadcv')}}">{{__('jobs.download_cv')}}</a></li>
       @endif
   </ul>
        @empty
        @endforelse

</div>
@endif

<div class="panel panel-default">
   <div class="panel-heading">{{__('groups.title')}} @if($my_profile) <a href="javascript:;" data-toggle="modal" data-target="#profileHobbies"><i>{{ __('home.edit') }}</i></a> @endif</div>

   <ul class="list-group" style="max-height: 300px; overflow-x: auto">
       @if($user->hobbies()->count() == 0)
           <li class="list-group-item">{{__('groups.no_group')}}</li>
       @else
           @foreach($user->hobbies()->get() as $hobby)
               <li class="list-group-item">{{ $hobby->hobby->name }}</li>
           @endforeach
       @endif
   </ul>
</div>




@if($my_profile)
<div class="modal fade" id="profileInformation" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
   <div class="modal-dialog">
       <div class="modal-content">
           <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                   <span aria-hidden="true">&times;</span>
               </button>
               <h5 class="modal-title">{{__('home.your_information')}}</h5>
           </div>

           <div class="modal-body">
               <form id="form-profile-information">
                   <!--add Location -->
                   <div class="form-group">

                       <div class="row">

                           <div class="col-md-12 map-place"></div>
                       </div>
                       <div class="clearfix"></div>
                   </div>
                   <div class="row">
                       <div class="col-md-6">
                           <div class="form-group">
                               <label>{{__('home.gender')}}</label>
                               <select class="form-control " name="sex">
                                   <option value="0" @if($user->sex == 0){{ 'selected' }}@endif>{{__('home.male')}}</option>
                                   <option value="1" @if($user->sex == 1){{ 'selected' }}@endif>{{__('home.female')}}</option>
                               </select>
                           </div>
                       </div>
                       <div class="col-md-6">
                           <div class="form-group">
                               <label>{{__('home.birthday')}}</label>
                               <div class="input-group">
                                   <span class="input-group-addon" id="basic-addon1"><i class="fa fa-birthday-cake"></i></span>
                                   <input type="text" class="form-control datepicker" name="birthday" value="@if($user->birthday){{ $user->birthday->format('Y-m-d') }}@endif" aria-describedby="basic-addon1" data-date-format="yyyy-mm-dd">
                               </div>
                           </div>
                       </div>
                       <div class="col-md-12">
                           <div class="form-group">
                               <label>{{__('home.phone')}}:</label>
                               <div class="input-group">
                                   <span class="input-group-addon" id="basic-addon1"><i class="fa fa-mobile"></i></span>
                                   <input type="text" class="form-control" name="phone" value="{{ $user->phone }}" aria-describedby="basic-addon1">
                               </div>
                           </div>
                       </div>
                       <div class="col-md-6">
                           <div class="form-group">
                               <label>{{__('home.country')}}</label>
                               <select class="form-control countries" name="country" id="countryId">
                                   <option value="0">{{__('home.chooseCountry')}}</option>
                                  
                               </select>
                           </div>
                       </div>
                       <div class="col-md-6">
                           <div class="form-group">
                               <label>{{__('home.city')}}</label>
                               <select class="form-control states" name="state" id="stateId">
                                   @if($user->location) 
                                   <option value="{{$user->location->city->name}}" selected>{{$user->location->city->name}}</option>
                                    @endif
                               </select>
                           </div>
                       </div>

                   </div>
                   <div class="form-group">
                       <label>Bio</label>
                       <textarea name="bio" class="form-control">{{ $user->bio }}</textarea>
                   </div>
               </form>
           </div>

           <div class="modal-footer">
               <button type="button" class="btn btn-success" onclick="saveInformation()">{{__('home.save')}}</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">{{__('home.close')}}</button>
           </div>
       </div>
   </div>
</div>

<div class="modal fade" id="work_experience" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
   <div class="modal-dialog">
       <div class="modal-content">
           <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                   <span aria-hidden="true">&times;</span>
               </button>
               <h5 class="modal-title">{{__('jobs.work_experience')}}</h5>
           </div>
           <form id="form-profile-work" method="post" action="{{ url('workexp/'.$user->workExperience()->get()[0]->id.'/update') }}" enctype="multipart/form-data">
               {{ csrf_field() }}

               <div class="modal-body">
                   <div class="form-group">
                       <div class="row">
                           <div class="col-xs-12">
                               <label for="formFileLg" class="form-label">{{__('jobs.your_cv')}}</label>
                               <input class="form-control form-control-lg" name="cv" id="formFileLg" type="file" value="" />
                           </div>
                       </div>
                       <div class="row">
                           <div class="col-xs-12">
                               <label for="formFileLg" class="form-label">{{__('jobs.last_work')}}</label>
                           
                              <textarea class="form-control" name="workexpereince">{{$user->workExperience()->get()[0]->workexpereince}}</textarea>
                          
                           </div>
                       </div>
                   </div>
               </div>

               <div class="modal-footer">
                   <button type="submit" class="btn btn-success">{{__('home.save')}}</button>
                   <button type="button" class="btn btn-default" data-dismiss="modal">{{__('home.close')}}</button>
               </div>
           </form>
       </div>
   </div>
</div>

<div class="modal fade" id="profileHobbies" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
   <div class="modal-dialog">
       <div class="modal-content">
           <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                   <span aria-hidden="true">&times;</span>
               </button>
               <h5 class="modal-title">{{__('groups.your_groups')}}</h5>
           </div>
           <form id="form-profile-hobbies" method="post" action="{{ url('/'.$user->username.'/save/hobbies') }}">
               {{ csrf_field() }}

               <div class="modal-body">
                   <div class="form-group">
                       <label>{{__('groups.title')}}:</label>
                       <div class="row">
                           <div class="col-xs-12">
                               <select class="form-control select2-multiple" name="hobbies[]" multiple="multiple" style="width: 100%">
                                   @foreach($hobbies as $hobby)
                                       <option value="{{ $hobby->id }}" @if($user->hasHobby($hobby->id)){{ 'selected' }}@endif>{{ $hobby->name }}</option>
                                   @endforeach
                               </select>
                           </div>
                       </div>
                   </div>
               </div>

               <div class="modal-footer">
                   <button type="submit" class="btn btn-success">{{__('home.save')}}</button>
                   <button type="button" class="btn btn-default" data-dismiss="modal">{{__('home.close')}}</button>
               </div>
           </form>

       </div>
   </div>
</div>
<div class="modal fade" id="profileRelationship" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
   <div class="modal-dialog">
       <div class="modal-content">
           <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                   <span aria-hidden="true">&times;</span>
               </button>
               <h5 class="modal-title">New Relationship</h5>
           </div>
           <form id="form-profile-hobbies" method="post" action="{{ url('/'.$user->username.'/save/relationship') }}">
               {{ csrf_field() }}

               <div class="modal-body">
                   @if($user->messagePeopleList()->count() == 0)
                       You don't have follower which they follows you
                   @else

                   <div class="form-group">
                       <label>Person:</label>
                       <div class="row">
                           <div class="col-xs-12">
                               <select class="form-control" name="person" style="width: 100%">
                                   @foreach($user->messagePeopleList()->get() as $fr)
                                       <option value="{{ $fr->follower->id }}">{{ $fr->follower->name }}</option>
                                   @endforeach
                               </select>
                           </div>
                       </div>
                   </div>

                   <div class="form-group">
                       <label>Relation:</label>
                       <div class="row">
                           <div class="col-xs-12">
                               <select class="form-control" name="relation" style="width: 100%">
                                   <option value="0">Mother</option>
                                   <option value="1">Father</option>
                                   <option value="2">Spouse</option>
                                   <option value="3">Sister</option>
                                   <option value="4">Brother</option>
                                   <option value="5">Relative</option>
                               </select>
                           </div>
                       </div>
                   </div>
                   @endif

               </div>

               <div class="modal-footer">
                   @if($user->messagePeopleList()->count() > 0)
                   <button type="submit" class="btn btn-success">{{__('home.save')}}</button>
                   @endif
                   <button type="button" class="btn btn-default" data-dismiss="modal">{{__('home.close')}}</button>
               </div>
           </form>

       </div>
   </div>
</div>

@endif
@push('countryfooter')
 <script src="{{ asset('js/countrystatecity.js')}}"></script>
@endpush