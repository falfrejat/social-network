@extends('layouts.guest-password')

@section('content')
    <main class="login-form">
        <div class="cotainer">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card" style="background-color: #fff;">
                        <div class="h3-guest">Reset Password</div>
                        <div class="card-body" style="padding: 0px 30px 40px 40px;">


                            <form action="{{ route('forget.password.post') }}" method="POST">
                                {{ csrf_field() }}
                                <div class="row">
                                <div class="form-group col-md-12">
                                    <label for="email" class="control-label">E-Mail Address</label>
                                    <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon1"><i class="fa fa-envelope"></i> </span>
                                        <input type="text" id="email_address" class="form-control" name="email" required autofocus>
                                    </div>
                                    @if ( $errors->has('email'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-2">
                                        <button type="submit" class="btn btn-primary btn-login">
                                            Send Password Reset Link
                                        </button>

                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection