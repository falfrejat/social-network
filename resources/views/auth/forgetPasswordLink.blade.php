@extends('layouts.guest-password')

@section('content')
{{--    @if ($errors->any())--}}
{{--        <div class="alert alert-danger">--}}
{{--            <ul>--}}
{{--                @foreach ($errors->all() as $error)--}}
{{--                    <li>{{ $error }}</li>--}}
{{--                @endforeach--}}
{{--            </ul>--}}
{{--        </div>--}}
{{--    @endif--}}
<main class="login-form">
    <div class="cotainer">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card" style="background-color: #fff;">
                    <div class="h3-guest">Reset Password</div>
                    <div class="card-body" style="padding: 0px 30px 40px 40px;">

                        <form action="{{ route('reset.password.post') }}" method="POST">

                            {{ csrf_field() }}
                            <input type="hidden" name="token" value="{{ $token }}">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label for="email" class="control-label">E-Mail Address</label>
                                    <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon1"><i class="fa fa-envelope"></i> </span>
                                        <input type="text" id="email_address" class="form-control" name="email" required autofocus>
                                    </div>
                                    @if ( $errors->has('email'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                    @endif
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="password" class="control-label">Password</label>

                                    <div  class="input-group">
                                        <span class="input-group-addon" id="basic-addon1"><i class="fa fa-unlock-alt"></i> </span>
                                        <input type="password" id="password" class="form-control" name="password" required autofocus>
                                    </div>
                                    @if ( $errors->has('password'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="password" class="control-label">Confirm Password</label>

                                    <div  class="input-group">
                                        <span class="input-group-addon" id="basic-addon1"><i class="fa fa-unlock-alt"></i> </span>
                                        <input type="password" id="password-confirm" class="form-control" name="password_confirmation" required autofocus>

                                    </div>
                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-2">
                                        <button type="submit" class="btn btn-primary btn-login">
                                            Reset Password
                                        </button>

                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="sub-title">
                                    <hr />
                                    <!-- <span>or</span>-->
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
    @endsection