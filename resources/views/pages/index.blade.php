@extends('layouts.app')

@section('content')
    <div class="h-20"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                @include('widgets.sidebar')
            </div>
            <div class="col-md-9">

                <div class="content-page-title">
                    <i class="fa fa-users"></i> {{__('groups.pages_title')}}
                </div>
                <div class="content-page-title">
                    <a href="{{url('pages/create')}}" class="btn btn-primary">{{__('groups.create_page')}}</a>
                </div>

                @if($pages->count() == 0)
                    <div class="alert-message alert-message-default">
                        <h4>{{__('groups.you_dont_have_pages')}}</h4>
                    </div>
                @else
                    <div class="row">
                        <div class="panel panel-default">
                            <div class="panel-heading"><h4>Pages you own</h4></div>

                            <div class="panel-body">

                                @foreach($pages->get() as $page)

                                    <div class="col-sm-6 col-md-4">
                                        <a class="bs-box" href="{{ url('/page/'.$page->id) }}">
                                            <h3>{{ $page->name }}</h3>
                                            {{--                                    <p>People in {{ $city->name }}: {{ $group->countPeople($city->id) }}</p>--}}
                                        </a>
                                    </div>

                                @endforeach
                            </div>
                        </div>



                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('footer')

@endsection