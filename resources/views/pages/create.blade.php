@extends('layouts.app')

@section('content')
    <div class="h-20"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                @include('widgets.sidebar')
            </div>

            <div class="col-md-9">
                @if(Session::has('alert-success'))
                    <div class="alert alert-success"><i class="fa fa-check" aria-hidden="true"></i> <strong>{!! session('alert-success') !!}</strong></div>
                @endif
                @if(Session::has('alert-danger'))
                    <div class="alert alert-danger"><i class="fa fa-times" aria-hidden="true"></i> <strong>{!! session('alert-danger') !!}</strong></div>
                @endif
            </div>
            <div class="col-md-9">

                <div class="content-page-title">
                    <i class="fa fa-users"></i> {{__('groups.pages_title')}}
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">{{__('groups.create_page')}}</div>

                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('pages/create') }}"  enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">{{__('groups.page_name')}}</label>
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name"  >
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="formFileLg" class="col-md-4 control-label">{{__('groups.profile_page')}}</label>
                                <div class="col-md-6">
                                    <input class="form-control form-control-lg" name="profile_path" id="profile_path" type="file" value="" />
                                    @if ($errors->has('profile_path'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('profile_path') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="formFileLg" class="col-md-4 control-label">{{__('groups.description')}}</label>
                                <div class="col-md-6">
                                    <textarea class="form-control form-control-lg" name="description" id="description"  value="" >

                                    </textarea>
                                    @if ($errors->has('profile_path'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('profile_path') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{__('groups.create')}}
                                    </button>
                                </div>
                            </div>
                        </form>


                    </div>
                </div>





            </div>
        </div>
    </div>
@endsection

@section('footer')

@endsection