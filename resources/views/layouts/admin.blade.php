<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>FXTAMER Admin Panel</title>

    <!-- Main Styles -->
    <link rel="stylesheet" href="{{ asset('admin/assets/styles/style.min.css') }}">

    <!-- Material Design Icon -->
    <link rel="stylesheet" href="{{ asset('admin/assets/fonts/material-design/css/materialdesignicons.css')}}">
    <!-- mCustomScrollbar -->
    <link rel="stylesheet" href="{{ asset('admin/assets/plugin/mCustomScrollbar/jquery.mCustomScrollbar.min.css')}}">
    <!-- Waves Effect -->
    <link rel="stylesheet" href="{{ asset('admin/assets/plugin/waves/waves.min.css')}}">
    <!-- Sweet Alert -->
    <link rel="stylesheet" href="{{ asset('admin/assets/plugin/sweet-alert/sweetalert.css')}}">


    @stack('header-script')

</head>

<body>
<div class="main-menu">
    <header class="header">
        <a href="{{url('admin/home')}}" class="logo"><i class="ico mdi "></i><img src="{{ asset('images/trade-logo.png') }}" width="150px" ></a>
        <button type="button" class="button-close fa fa-times js__menu_close"></button>
        <div class="user">
            <a href="#" class="avatar"><img src="{{url('admin/assets/images/avatar-4.jpg')}}" alt=""><span class="status online"></span></a>
            <h5 class="name"><a href="#">Admin</a></h5>
            <h5 class="position">Administrator</h5>
            <!-- /.name -->
            <div class="control-wrap js__drop_down">
                <i class="fa fa-caret-down js__drop_down_button"></i>
                <div class="control-list">

                    <div class="control-item"><a href="{{route('admin.logout')}}"><i class="fa fa-sign-out"></i> Log out</a></div>
                </div>
                <!-- /.control-list -->
            </div>
            <!-- /.control-wrap -->
        </div>
        <!-- /.user -->
    </header>
    <!-- /.header -->
    <div class="content">

        <div class="navigation">
            <h5 class="title">Navigation</h5>
            <!-- /.title -->
            <ul class="menu js__accordion">
                <li class="current">
                    <a class="waves-effect" href="{{url('admin/home')}}"><i class="menu-icon mdi mdi-view-dashboard"></i><span>Dashboard</span></a>
                </li>
                <li>
                    <a class="waves-effect parent-item js__control" href="#"><i class="menu-icon mdi mdi-face-profile"></i><span>Users</span><span class="menu-arrow fa fa-angle-down"></span></a>
                    <ul class="sub-menu js__content">
                        <li><a href="{{route('admin.users')}}">Users List</a></li>
                    </ul>
                    <!-- /.sub-menu js__content -->
                </li>
                <li>
                    <a class="waves-effect parent-item js__control" href="#"><i class="menu-icon mdi mdi-cube-outline"></i><span>States</span><span class="menu-arrow fa fa-angle-down"></span></a>
                    <ul class="sub-menu js__content">
                        <li><a href="{{route('admin.countries')}}">Countries</a></li>
                        <li><a href="{{route('admin.cities')}}">Cities</a></li>
                    </ul>
                    <!-- /.sub-menu js__content -->
                </li>
                <li>
                    <a class="waves-effect parent-item js__control" href="#"><i class="menu-icon mdi mdi-textbox"></i><span>Posts</span><span class="menu-arrow fa fa-angle-down"></span></a>
                    <ul class="sub-menu js__content">
                        <li><a href="{{url('admin/posts')}}">Posts</a></li>
                        <li><a href="{{url('admin/comments')}}">Comments</a></li>

                    </ul>
                    <!-- /.sub-menu js__content -->
                </li>
                <li>
                    <a class="waves-effect" href="{{route('admin.jobs')}}"><i class="menu-icon mdi mdi-cube-outline"></i><span>Jobs</span><span class="menu-arrow fa"></span></a>
                </li>
                <li>
                    <a class="waves-effect" href="{{route('admin.library')}}"><i class="menu-icon mdi mdi-cube-outline"></i><span>Library</span><span class="menu-arrow fa"></span></a>
                </li>
                <li>
                    <a class="waves-effect" href="{{route('admin.logger')}}"><i class="menu-icon mdi mdi-cube-outline"></i><span>Logger</span><span class="menu-arrow fa"></span></a>
                </li>
                <li>
                    <a class="waves-effect" href="{{route('admin.settings')}}"><i class="menu-icon mdi mdi-cube-outline"></i><span>Settings</span><span class="menu-arrow fa "></span></a>
                </li>
               <!-- <li>
                    <a class="waves-effect parent-item js__control" href="#"><i class="menu-icon mdi mdi-cube-outline"></i><span>Social</span><span class="menu-arrow fa fa-angle-down"></span></a>
                    <ul class="sub-menu js__content">
                        <li><a href="{{url('admin/social')}}">Facebook</a></li>
                    </ul>

                </li>-->
            </ul>

        </div>
        <!-- /.navigation -->
    </div>
    <!-- /.content -->
</div>
<!-- /.main-menu -->

<div class="fixed-navbar">
    <div class="pull-left">
        <button type="button" class="menu-mobile-button glyphicon glyphicon-menu-hamburger js__menu_mobile"></button>
        <h1 class="page-title">Home</h1>
        <!-- /.page-title -->
    </div>
    <!-- /.pull-left -->
    <div class="pull-right">
        <div class="ico-item">
            <a href="#" class="ico-item mdi mdi-magnify js__toggle_open" data-target="#searchform-header"></a>
            <form action="#" id="searchform-header" class="searchform js__toggle"><input type="search" placeholder="Search..." class="input-search"><button class="mdi mdi-magnify button-search" type="submit"></button></form>
            <!-- /.searchform -->
        </div>

        <a href="{{route('admin.logout')}}" class="ico-item mdi mdi-logout "></a><!--js__logout-->
    </div>
    <!-- /.pull-right -->
</div>
<!-- /.fixed-navbar -->



<!-- /#message-popup -->
<div id="wrapper">
    <div class="main-content">
        <div class="row small-spacing">
            <div class="col-xs-12">
            @yield('content')
            </div>
            <!-- /.col-xs-12 -->
        </div>
        <!-- /.row -->

        <!-- .row -->
        <footer class="footer">
            <ul class="list-inline">
                <li>2021 © BIS.</li>
               <!-- <li><a href="#">Privacy</a></li>
                <li><a href="#">Terms</a></li>
                <li><a href="#">Help</a></li>-->
            </ul>
        </footer>
    </div>
    <!-- /.main-content -->
</div><!--/#wrapper -->
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="{{ asset('admin/assets/script/html5shiv.min.js')}}"></script>
<script src="{{ asset('admin/assets/script/respond.min.js')}}"></script>
<![endif]-->
<!--
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!--<script src="{{ asset('admin/assets/scripts/jquery.min.js')}}"></script>-->
<script src="{{ asset('admin/assets/scripts/jquery-3.6.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.js"></script>
<script src="{{ asset('admin/assets/scripts/modernizr.min.js')}}"></script>
<script src="{{ asset('admin/assets/plugin/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('admin/assets/plugin/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<script src="{{ asset('admin/assets/plugin/nprogress/nprogress.js')}}"></script>
<script src="{{ asset('admin/assets/plugin/sweet-alert/sweetalert.min.js')}}"></script>
<script src="{{ asset('admin/assets/plugin/waves/waves.min.js')}}"></script>


<!-- Data Tables -->
<script src="{{ asset('admin/assets/plugin/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('admin/assets/plugin/datatables/media/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{ asset('admin/assets/plugin/datatables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{ asset('admin/assets/plugin/datatables/extensions/Responsive/js/responsive.bootstrap.min.js')}}"></script>
<script src="{{ asset('admin/assets/scripts/datatables.demo.min.js')}}"></script>

<script src="{{ asset('admin/assets/scripts/main.min.js')}}"></script>

<script type="text/javascript" src="https://cdn.rawgit.com/mervick/emojionearea/master/dist/emojionearea.min.js"></script>

@stack('footer-script')
</body>
</html>