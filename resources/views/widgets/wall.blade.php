@section('header')



@endsection

<div class="clearfix"></div>
@if($user->id == Auth::user()->id)
    @php $locale = session()->get('locale'); @endphp
    <style>
        * {
            font-family: Arial, Helvetica, san-serif;
        }

        .span6 {
            float: left;
            width: 100%;
            padding: 1%;
        }
        .btn-sm{
            width: 100%;
        }
        .dropdown-menu{
            padding: 0px;
        }
    </style>
<!--New post -->
<div class="panel panel-default new-post-box">
    <div class="panel-body">
        <form id="form-new-post">
            <input type="hidden" name="page_id" value="{{ $wall['new_post_page_id'] }}">
            <input type="hidden" name="group_id" value="{{ $wall['new_post_group_id'] }}">
            <div class="form-group">
                <select class="form-control" id="public" name="public">
                    <option value="0">private</option>
                    <option value="1">public</option>
                </select>
            </div>
            <div class="span6">
                <textarea name="content" placeholder="Share what you think or photos" class="post-mention" id="emojionearea1"></textarea>
            </div>

            <div class="image-area">
                <a href="javascript:;" class="image-remove-button" onclick="removePostImage()"><i class="fa fa-times-circle"></i></a>
                <img src="" />
            </div>
            <hr />
            <div class="row">
                <div class="col-xs-4 {{($locale == 'ar')?'pull-right':''}}">
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{__('home.file')}}
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <button type="button" class="btn btn-default btn-add-image btn-sm {{($locale == 'ar')?'pull-right':''}}" onclick="uploadPostImage()">
                                <i class="fa fa-image"></i> {{__('home.add_image_video')}}
                            </button>
                            <button type="button" class="btn btn-default btn-add-image btn-sm {{($locale == 'ar')?'pull-right':''}}" onclick="uploadPostImage()">
                                <i class="fa fa-image"></i> {{__('home.add_doc')}}
                            </button>
                        </div>
                    </div>

                    <input type="file" accept="image/*,video/*,.docx,.pdf" class="image-input" name="photo" onchange="previewPostImage(this)">
                </div>
                <div class="col-xs-4 {{($locale == 'ar')?'pull-right':''}}">
                    <div class="loading-post">
                        <img src="{{ asset('images/rolling.gif') }}" alt="">
                    </div>
                </div>
                <div class="col-xs-4 {{($locale == 'ar')?'pull-left':''}}">
                    <button type="button" class="btn btn-primary btn-submit {{($locale == 'ar')?'pull-left':'pull-right'}} " onclick="newPost()">
                         {{__('home.post')}}
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endif

<div class="post-list-top-loading">
    <img src="{{ asset('images/rolling.gif') }}" alt="">
</div>
<div class="post-list">

</div>
<div class="post-list-bottom-loading">
    <img src="{{ asset('images/rolling.gif') }}" alt="">
</div>

<div class="modal fade " id="likeListModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title">  {{__('home.likes')}}</h5>
            </div>

            <div class="user_list">

            </div>
        </div>
    </div>
</div>
<!--save post modal -->
<div class="modal fade " id="savePostModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title">  {{__('home.message')}}</h5>
            </div>

            <div class="modal-body">
                {{__('home.post_saved')}}
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>



