<div class="menu">
    <ul class="list-group">
        <li class="list-group-item">
            <a href="{{ url('/') }}" class="menu-home">
                <i class="fa fa-home"></i>
                {{__('home.home')}}
            </a>
        </li>

        <li class="list-group-item">
            <a href="{{ url('/groups') }}" class="menu-groups">
                <i class="fa fa-users"></i>
                {{__('home.groups')}}
            </a>
        </li>
        <li class="list-group-item">
            <a href="{{ url('/jobs') }}" class="menu-nearby">
                <i class="fa fa-empire"></i>
                {{__('home.jobs')}}
            </a>
        </li>
        <li class="list-group-item">
            <a href="{{ url('/direct-messages') }}" class="menu-dm">
                <i class="fa fa-commenting"></i>
                Direct Messages
            </a>
        </li>
        <li class="list-group-item">
            <a href="{{ url('/pages') }}" class="menu-dm">
                <i class="fa fa-file-text"></i>
                {{__('home.pages')}}
            </a>
        </li>
        <li class="list-group-item">
            <a href="{{ url('/saved-posts') }}" class="menu-dm">
                <i class="fa fa-angle-double-down"></i>
                {{__('home.savedposts')}}
            </a>
        </li>
    </ul>
</div>