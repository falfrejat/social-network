<div class="panel panel-default">
    <div class="panel-heading"><a href="{{url('/library')}}">{{__('home.library')}}</a> </div>

    <ul class="list-group" style="max-height: 300px; overflow-x: auto">
        @if($books->count() == 0)
            <li class="list-group-item">{{__('library.nobook')}}</li>
        @else
            @foreach($books as $key=>$book)
                <li class="list-group-item"><a href="{{url('library/book/'.$book->id)}}">{{ $book->title }}</a></li>
            @endforeach
        @endif
    </ul>
</div>