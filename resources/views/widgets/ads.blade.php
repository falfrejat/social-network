<div class="panel panel-default">
    <div class="panel-heading"><a href="{{url('/ads')}}">{{__('home.market_places')}}</a> </div>

    <ul class="list-group" style="max-height: 300px; overflow-x: auto">
        @if($ads->count() == 0)
            <li class="list-group-item">{{__('ads.noads')}}</li>
        @else
            @foreach($ads as $key=>$ad)
                <li class="list-group-item"><a href="{{url('ad/'.$ad->id)}}">{{ $ad->title }}</a></li>
            @endforeach
        @endif
    </ul>
</div>