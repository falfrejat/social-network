@php $locale = session()->get('locale'); @endphp
<div class="panel panel-default panel-post " id="panel-post-{{ $post->id }}">
    <div class="panel-body">
        @if($wall_type==3)
        <!--page-->
        <div class=" {{($locale=='ar')?'pull-right':'pull-left'}}">
            <a href="#">
                <img class="media-object img-circle post-profile-photo" src="{{ $page->getPhoto(60,60) }}">
            </a>
        </div>
        <div class="{{($locale=='ar')?'pull-right':'pull-left'}} info">
            <a href="{{ url('/page/'.$page->username) }}" class="name">{{ $page->name }}</a>
            <a href="{{url('/page/'.$page->username) }}" class="username">{{ '@'.$page->username }}</a>
            <a href="{{ url('/post/'.$post->id) }}" class="date"><i class="fa fa-clock-o" aria-hidden="true"></i> {{ ($post->created_at)?$post->created_at->diffForHumans():'' }}</a>
        </div>
        @else
        <div class=" {{($locale=='ar')?'pull-right':'pull-left'}}">
            <a href="#">
                <img class="media-object img-circle post-profile-photo" src="{{ $post->user->getPhoto(60,60) }}">
            </a>
        </div>
        <div class="{{($locale=='ar')?'pull-right':'pull-left'}} info">
            <a href="{{ url('/'.$post->user->username) }}" class="name">{{ $post->user->name }}</a>
            <a href="{{ url('/'.$post->user->username) }}" class="username">{{ '@'.$post->user->username }}</a>
            <a href="{{ url('/post/'.$post->id) }}" class="date"><i class="fa fa-clock-o" aria-hidden="true"></i> {{ ($post->created_at)?$post->created_at->diffForHumans():'' }}</a>
        </div>
        @endif
        <div class="{{($locale=='ar')?'pull-left':'pull-right'}} like-box">
            <a href="javascript:;" onclick="likePost({{ $post->id }})" class="like-text">
                @if($post->checkLike($user->id))
                    <i class="fa fa-heart"></i> <span>Unlike!</span>
                @else
                    <i class="fa fa-heart-o"></i> <span>Like!</span>
                @endif
            </a>
            <div class="clearfix"></div>
            <a href="javascript:;" class="all_likes" onclick="showLikes({{ $post->id }})"><span>{{ $post->getLikeCount() }} @if($post->getLikeCount() > 1){{ 'likes' }}@else{{ 'like' }}@endif</span></a>
        </div>
        <div class="clearfix"></div>
        <span>
            @if($post->checkOwner($user->id))
                <div class="{{($locale=='ar')?'navbar-left':'navbar-right'}}">
                    <div class="dropdown">
                        <button class="btn btn-link btn-xs dropdown-toggle" type="button" id="dd1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <i class="fa fa-cog" aria-hidden="true"></i>
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dd1" style="float: right;">
                            <li><a href="{{url('post/edit/'.$post->id)}}" ><i class="fa fa-fw fa-pencil" aria-hidden="true"></i> {{__('home.edit')}}</a></li>
                            <li><a href="javascript:;" onclick="deletePost({{ $post->id }})"><i class="fa fa-fw fa-trash" aria-hidden="true"></i> {{__('home.delete')}}</a></li>
                        </ul>
                    </div>
                </div>
            @else
            <!-- if not owner post then user can save post -->
                <div class="{{($locale=='ar')?'navbar-left':'navbar-right'}}">
                    <div class="dropdown">
                        <button class="btn btn-link btn-xs dropdown-toggle" type="button" id="dd1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <i class="fa fa-cog" aria-hidden="true"></i>
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dd1" style="float: right;">
                            <!--save post -->
                            <li><a href="javascript:;" onclick="savePost({{ $post->id }})" ><i class="fa fa-fw " aria-hidden="true"></i> {{__('home.save_post')}}</a></li>
                        </ul>
                    </div>
                </div>
            @endif
        </span>
        <hr class="fix-hr">
        <div class="post-content post-content-s">
             @emojione($post->content)
            @if($post->hasVideo())
                @foreach($post->images()->get() as $image)
                    <video controls width="100%" height="100%">
                        <source src="{{$image->getURLVideo()}}" type='video/mp4 ' >
                        <source src="{{$image->getURLVideo()}}" type="video/webm">
                    </video>
                @endforeach
            @elseif($post->hasImage() && !$post->hasDoc())
                @foreach($post->images()->get() as $image)
                    <a data-fancybox="gallery" href="{{ $image->getURL() }}" data-caption="{{ $post->content }}"><img class="img-responsive post-image" src="{{ $image->getURL() }}"></a>
                @endforeach
            @elseif($post->hasDoc() )
                @foreach($post->images()->get() as $image)
{{--                    <a  href="{{ $image->getURLDoc() }}" data-caption="{{ $post->content }}">{{ $image->getURLDoc() }}</a>--}}
                    <i class="fa fa-file-pdf-o"></i> <a target="_blank" href="{{url('post/'.$image->image_path.'/downloadbook')}}">{{__('library.show_doc')}}</a></h4>
                @endforeach
            @endif
        </div>
        <hr class="fix-hr">
        <div class="comments-title">
            @include('widgets.post_detail.comments_title')
        </div>
        <div class="post-comments">
            @foreach($post->comments()->limit($comment_count)->orderBY('id', 'DESC')->with('user')->get()->reverse() as $comment)
                @include('widgets.post_detail.single_comment')
            @endforeach

        </div>
<style>
    * {
        font-family: Arial, Helvetica, san-serif;
    }
    .span7 {
        float: left;
        width: 90%;
        padding: 1%;
    }
    .emojionearea-editor:not(.inline) {
        min-height: 8em!important;
    }
    .media, .media-body {
        overflow: inherit;
    }
</style>
        <div class="clearfix"></div>
        <!-- new comment -->
        <div class="media post-write-comment ">
            <form id="form-new-comment">
                <div class="{{($locale=='ar')?'pull-right':'pull-left'}}">
                    <a href="{{ url('/'.$user->username) }}">
                        <img class="media-object img-circle" src="{{ $user->getPhoto(60,60) }}">
                    </a>
                </div>
                <div class="media-body span7">
                    <textarea class="form-control emojioneareaComment" rows="1" placeholder="Comment" ></textarea>
                </div>
                <button type="button" class="btn btn-default btn-xs pull-right" onclick="submitComment({{ $post->id }})">
                    {{__('home.add_comment')}}
                </button>
            </form>
        </div>
    </div>
</div>
