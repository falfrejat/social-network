@php $locale = session()->get('locale'); @endphp
<div class="panel panel-default new-post-box" >
    <div class="panel-body">
        <div class=" {{($locale=='ar')?'pull-right':'pull-left'}}">
            <a href="#">
                <img class="media-object img-circle post-profile-photo" src="{{ $post->user->getPhoto(60,60) }}">
            </a>
        </div>
        <div class="{{($locale=='ar')?'pull-right':'pull-left'}} info">
            <a href="{{ url('/'.$post->user->username) }}" class="name">{{ $post->user->name }}</a>
            <a href="{{ url('/'.$post->user->username) }}" class="username">{{ '@'.$post->user->username }}</a>
            <a href="{{ url('/post/'.$post->id) }}" class="date"><i class="fa fa-clock-o" aria-hidden="true"></i> {{ ($post->created_at)?$post->created_at->diffForHumans():'' }}</a>
        </div>

        <div class="clearfix"></div>

        <hr class="fix-hr">
{{--        <div class="post-content post-content-s">--}}
{{--            <textarea col="7" rows="5" class="form-control" id="content"> @emojione($post->content)</textarea>--}}

{{--            @if($post->hasVideo())--}}
{{--                @foreach($post->images()->get() as $image)--}}
{{--                    <video controls width="100%" height="100%">--}}
{{--                        <source src="{{$image->getURLVideo()}}" type='video/mp4' >--}}
{{--                        <source src="{{$image->getURLVideo()}}" type="video/webm">--}}
{{--                    </video>--}}
{{--                @endforeach--}}
{{--            @elseif($post->hasImage())--}}
{{--                @foreach($post->images()->get() as $image)--}}
{{--                    <a data-fancybox="gallery" href="{{ $image->getURL() }}" data-caption="{{ $post->content }}"><img class="img-responsive post-image" src="{{ $image->getURL() }}"></a>--}}
{{--                @endforeach--}}
{{--            @endif--}}
{{--        </div>--}}
        <form id="form-edit-post">
            <input type="hidden" name="group_id" value="{{ $wall['new_post_group_id'] }}">
            <div class="span6">
                <textarea name="content" placeholder="Share what you think or photos" id="emojionearea1">@emojione($post->content) </textarea>
            </div>
            <div class="image-area">
                <a href="javascript:;" class="image-remove-button" onclick="removePostImage()"><i class="fa fa-times-circle"></i></a>
                <img src="" />
            </div>
            <hr />
            <div class="row">
                <div class="col-xs-4 {{($locale == 'ar')?'pull-right':''}}">
                    <button type="button" class="btn btn-default btn-add-image btn-sm {{($locale == 'ar')?'pull-right':''}}" onclick="uploadPostImage()">
                        <i class="fa fa-image"></i> {{__('home.add_image_video')}}
                    </button>
                    <input type="file" accept="image/*,video/*" class="image-input" name="photo" onchange="previewPostImage(this)">
                </div>
                <div class="col-xs-4 {{($locale == 'ar')?'pull-right':''}}">
                    <div class="loading-post">
                        <img src="{{ asset('images/rolling.gif') }}" alt="">
                    </div>
                </div>
                <div class="col-xs-4 {{($locale == 'ar')?'pull-left':''}}">
                    <button type="button" class="btn btn-primary btn-submit {{($locale == 'ar')?'pull-left':'pull-right'}}" onclick="editPost({{$post->id}})">
                        {{__('home.post')}}
                    </button>
                </div>
            </div>
        </form>



    </div>
</div>
