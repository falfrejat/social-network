@php $locale = session()->get('locale'); @endphp
@if(!($post->user_id == Auth::id() && $comment->ishide==1))
<div class="panel-default post-comment col-lg-12" id="post-comment-{{ $comment->id }}">
    <div class="panel-body ">
        <div class="{{($locale=='ar')?'pull-right':'pull-left'}} ">
            <a href="{{ url('/'.$comment->user->username) }}">
                <img class="media-object img-circle comment-profile-photo" src="{{ $comment->user->getPhoto(60,60) }}">
            </a>
        </div>
        <div class="{{($locale=='ar')?'pull-right':'pull-left'}}  comment-info">
            <a href="{{ url('/'.$comment->user->username) }}" class="name">{{ $comment->user->name }}</a>
            <a href="{{ url('/'.$comment->user->username) }}" class="username">{{ '@'.$comment->user->username }}</a>
            <span class="date"><i class="fa fa-clock-o" aria-hidden="true"></i> {{ $comment->created_at->diffForHumans() }}</span>
        @if($post->user_id == Auth::id())
           <div class="{{($locale=='ar')?'navbar-left':'navbar-right'}}">
               <div class="dropdown ml20 mr20">
                   <button class="btn btn-link btn-xs dropdown-toggle" type="button" id="dd1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                       <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
{{--                        <span class="caret"></span>--}}
                   </button>
                   <ul class="dropdown-menu" aria-labelledby="dd1" style="float: right;">
                       <!--delete comment -->
                       @if($post->user_id == Auth::id() || $comment->comment_user_id == Auth::id())
                           <li> <a href="javascript:;" class="remove {{($locale=='ar')?'pull-left':'pull-right'}}" onclick="removeComment({{ $comment->id }}, {{ $post->id }})"><i class="fa fa-times"></i> {{__('home.delete')}}</a></li>
                       @endif
                       <!--hide comment -->
                       @if($post->user_id == Auth::id() && $comment->comment_user_id != Auth::id())
                           <li> <a href="javascript:;" class="remove {{($locale=='ar')?'pull-left':'pull-right'}}" onclick="hideComment({{ $comment->id }}, {{ $post->id }})"><i class="fa fa-adjust"></i> {{__('home.hide')}}</a></li>
                       @endif
                   </ul>
               </div>
           </div>
            @endif
           <div class="clearfix"></div>
       </div>
       <br />
       <p>
           @emojione( $comment->comment)
       </p>
       <!--replies-->
{{--        <div class="reply-comments">--}}
{{--            @foreach($comments->replies()->orderBY('id', 'DESC')->with('user')->get()->reverse() as $reply)--}}
{{--                @include('widgets.post_detail.single_reply')--}}
{{--            @endforeach--}}
{{--        </div>--}}
{{--        <div class="media post-write-reply {{($locale=='ar')?'mr30':'ml30'}}">--}}
{{--            <form id="form-new-reply">--}}
{{--                <div class="{{($locale=='ar')?'pull-right':'pull-left'}}">--}}
{{--                    <a href="{{ url('/'.$user->username) }}">--}}
{{--                        <img class="media-object img-circle" src="{{ $user->getPhoto(40,40) }}">--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--                <div class="media-body span7 " >--}}
{{--                    <input class="form-control reply"  placeholder="Reply" id="reply" >--}}
{{--                    <input value="{{$comment->id}}" type="hidden" class="comment_id" >--}}
{{--                </div>--}}
{{--            </form>--}}
{{--        </div>--}}
   </div>


</div>
@endif


<div class="clearfix"></div>

<hr />