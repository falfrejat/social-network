@include('profile.widgets.card')
<div class="m-t-20"></div>
@include('widgets.menu')
<div class="m-t-20"></div>
<!--Ads only on homepage -->
@if(strpos($_SERVER['REQUEST_URI'], "home") !== false) @include('widgets.ads') @endif
<!--Libraries only on homepage -->
@if(strpos($_SERVER['REQUEST_URI'], "home") !== false) @include('widgets.library') @endif
<div class="m-t-20"></div>
@include('profile.widgets.user_follow_counts')