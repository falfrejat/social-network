@extends('layouts.app')

@section('content')
    <div class="h-20"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                @include('widgets.sidebar')
            </div>
            <div class="col-md-9">
                <div class="content-page-title">
                    <i class="fa fa-empire"></i> {{__('jobs.your_jobs')}}
                </div>
                <div class="row">

                    <ul class="list-group">
                        <li class="list-group-item">
                           <div class="col-lg-6">{{__('jobs.title')}}</div>
                            <div class="col-lg-6">{{$job->title}} </div>
                        </li>
                        <li class="list-group-item">
                            <div class="col-lg-6">{{__('jobs.company')}}</div>
                            <div class="col-lg-6">{{$job->company}} </div>
                        </li>
                        <li class="list-group-item content" >
                            <div class="col-lg-6">{{__('jobs.details')}}</div>
                            <div class="col-lg-6">{{$job->text}}</div>
                        </li>
                        <li class="list-group-item content" >
                            <div class="col-lg-6">{{__('jobs.tags')}}</div>

                            <div class="col-lg-6">
                                @if($job->tags !='' )
                                    @php ($tagsArr = explode(',',$job->tags))
                                    @foreach($tagsArr as $tagArr/*$i=0;$i<count($tagsArr);$i++*/)
                                        <a class="btn btn-default btn-sm" style="background-image: none">{{$tagArr}}</a>
                                    @endforeach
                                @else
                                    {{''}}
                                @endif


                            </div>
                        </li>
                        <li class="list-group-item content" >
                            <a class="btn  btn-primary" href="{{url('jobs/applyjob/'.$job->id)}}" id="job-button">{{__('jobs.apply_job')}}</a>
                        </li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
    <style>
        .list-group-item{
            height: 40px;
        }
        .content{
            height: auto;
            display: block;
            overflow: auto;
        }
    </style>

@endsection

@section('footer')

@endsection