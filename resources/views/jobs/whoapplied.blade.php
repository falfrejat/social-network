@extends('layouts.app')

@section('content')
    <div class="h-20"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                @include('widgets.sidebar')
            </div>
            <div class="col-md-9">
                <div class="content-page-title">
                    <i class="fa fa-empire"></i> {{__('jobs.who_apply')}}
                </div>
                <div class="row">
                    <table class="table-bordered table">
                    <tr>
                        <th>{{__('jobs.full_name')}}</th>
                        <th>{{__('jobs.email')}}</th>
                        <th>{{__('jobs.phone')}}</th>
                        <th>{{__('jobs.cv')}}</th>
                    </tr>
                        @foreach( $applieds as $applied)
                    <tr>
                        <td>{{$applied->full_name}}</td>
                        <td>{{$applied->email}}</td>
                        <td>{{$applied->phone}}</td>
                        <td> <a target="__blank" href="{{ url('job/'.$applied->id.'/downloadcv')}}"> {{__('jobs.downloadCV')}}</a></td>
                    </tr>
                        @endforeach

                    </table>






                </div>
            </div>
        </div>
    </div>
    <style>
        .list-group-item{
            height: 40px;
        }
        .content{
            height: auto;
            display: block;
            overflow: auto;
        }
    </style>

@endsection

@section('footer')

@endsection