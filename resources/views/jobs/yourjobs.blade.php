@extends('layouts.app')

@section('content')
    <div class="h-20"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                @include('widgets.sidebar')
            </div>
            <div class="col-md-9">
                <div class="content-page-title">
                    <i class="fa fa-empire"></i> {{__('jobs.your_jobs')}}
                </div>
                @if($jobs->count() == 0)
                    <div class="alert-message alert-message-default">
                        <h4>{{__('jobs.nojobs')}}</h4>
                    </div>
                @else
                    <div class="row">
                        @foreach($jobs as $key=>$job)

                                <div class="col-sm-6 col-md-4 bs-box ml10 mr10">
                                    <a  href="{{url('jobs/job/'.$job->id)}}">
                                        <h3>{{ $job->title }}</h3>
                                    </a>
                                    <p> {{ $job->company }}</p>
                                    <a href="{{url('jobs/who-applied/'.$job->id)}}" class="applied-job">{{__('jobs.who_apply')}}</a>
                                </div>
                        @endforeach
                    </div>
                @endif

            </div>
        </div>
    </div>
@endsection

@section('footer')

@endsection