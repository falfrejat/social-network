@extends('layouts.app')

@section('content')
    <div class="h-20"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                @include('widgets.sidebar')
            </div>
            <div class="col-md-9">

                <div class="content-page-title">
                    <i class="fa fa-empire"></i> {{__('home.jobs')}}
                </div>
                <div class="content-page-title">
                   <a href="{{url('jobs/create')}}" class="btn btn-primary">{{__('home.create_job')}}</a>

                    <a href="{{url('jobs/yourjobs')}}" class="btn btn-primary">{{__('jobs.your_jobs')}}</a>
                </div>
                <div class="content-page-blue-title">
                     {{__('jobs.latest_jobs')}}
                </div>
                @if($jobs->count() == 0)
                    <div class="alert-message alert-message-default">
                        <h4>{{__('jobs.nojobs')}}</h4>
                    </div>
                @else
                    <div class="row">
                        @foreach($jobs as $jey => $job)
                            <div class="col-sm-6 col-md-4">
                                <a class="bs-box" href="{{url('jobs/job/'.$job->id)}}">
                                    <h3>{{ $job->title }}</h3>
                                    <p> {{ $job->company }}</p>
                                </a>
                            </div>
                        @endforeach

                    </div>
                @endif

            </div>
        </div>
    </div>
@endsection

@section('footer')

@endsection