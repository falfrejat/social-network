@extends('layouts.app')

@section('content')
    <div class="h-20"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                @include('widgets.sidebar')
            </div>
            <div class="col-md-9">

                <div class="content-page-title">
                    <i class="fa fa-users"></i> {{__('groups.your_saved_posts')}}
                </div>


                @if($saved_posts->count() == 0)
                    <div class="alert-message alert-message-default">
                        <h4>{{__('groups.no_saved_posts')}}</h4>
                    </div>
                @else
                    <div class="row " >

                    @foreach($saved_posts->get() as $saved_post)
                        <div class="col-sm-12 col-md-12 bs-box">
                            <div class="col-md-4">
                                @if($saved_post->post->hasVideo())
                                    <video controls width="85%" height="90%">
                                        <source src="{{$saved_post->post->images()->get()[0]->getURLVideo()}}" type='video/mp4' >
                                        <source src="{{$saved_post->post->images()->get()[0]->getURLVideo()}}" type="video/webm">
                                    </video>
                                @elseif($saved_post->post->hasImage())
                                    <a data-fancybox="gallery" href="{{ $saved_post->post->images()->get()[0]->getURL() }}" data-caption="{{ $saved_post->post->content }}"><img class="img-responsive post-image" src="{{ $saved_post->post->images()->get()[0]->getURL() }}"></a>
                                @endif
                            </div>
                            <a class="{{($locale=='ar')?'text-right':'text-left'}}" href="{{ url('/post/'.$saved_post->post_id) }}">
                                <h3>{{($saved_post->post->content !='')?$saved_post->post->content:'Saved'}}</h3>
                                <div class=" {{($locale=='ar')?'pull-right':'pull-left'}}">
                                    <a href="#">
                                        <img class="media-object img-circle post-profile-photo" src="{{ $saved_post->post->user->getPhoto(40,40) }}">
                                    </a>
                                </div>
                                <div class="{{($locale=='ar')?'pull-right':'pull-left'}} info">
                                    <a href="{{ url('/'.$saved_post->post->user->username) }}" class="name">{{ $saved_post->post->user->name }}</a>
                                    <a href="{{ url('/'.$saved_post->post->user->username) }}" class="username">{{ '@'.$saved_post->post->user->username }}</a>
                                </div>
                            </a>
                        </div>
                    @endforeach

                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('footer')

@endsection