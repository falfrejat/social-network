<div class="message @if($message->sender_user_id == $user->id){{ 'message-right' }}@endif" id="chat-message-{{ $message->id }}">
    <div class="text">
        {{ $message->message }}
    </div>
    <a href="javascript:;" class="delete" onclick="deleteMessage({{ $message->id }})">{{__('home.delete')}}</a>
    <small class="text-left">{{ ($message->seen==1)?__('chat.seen'):'' }}</small>
    <small>{{ $message->created_at->format('d/m/Y H:i') }}</small>
</div>
<div class="clearfix"></div>