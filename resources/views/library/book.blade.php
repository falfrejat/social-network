@extends('layouts.app')

@section('content')
    <div class="h-20"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                @include('widgets.sidebar')
            </div>
            <div class="col-md-9">
                <div class="content-page-title">
                    <i class="fa fa-empire"></i> {{$book->title}}
                </div>
                <div class="row">

                    <ul class="list-group" style="height: auto">
                        <li class="list-group-item">
                           <div class="col-lg-6">{{__('jobs.title')}}</div>
                            <div class="col-lg-6">{{$book->title}} </div>
                        </li>
                        <li class="list-group-item" style="height: auto">
                            <h4><i class="fa fa-file-pdf-o"></i> <a target="_blank" href="{{url('library/'.$book->id.'/downloadbook')}}">{{__('library.show_book')}}</a></h4>
                        </li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
    <style>
        .list-group-item{
            height: 40px;
        }
        .content{
            height: auto;
            display: block;
            overflow: auto;
        }
    </style>

@endsection

@section('footer')

@endsection