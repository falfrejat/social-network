@extends('layouts.app')

@section('content')
    <div class="h-20"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                @include('widgets.sidebar')
            </div>
            <div class="col-md-9">
                <div class="content-page-title">
                    <i class="fa fa-empire"></i> {{__('library.your_books')}}
                </div>
                @if($books->count() == 0)
                    <div class="alert-message alert-message-default">
                        <h4>{{__('library.nobook')}}</h4>
                    </div>
                @else
                    <div class="row">
                        @foreach($books as $key=>$book)
                            <div class="col-sm-6 col-md-4">
                                <div class="bs-box" href="{{url('library/book/'.$book->id)}}">
                                    <h3>{{ $book->title }}</h3>
                                    <p> <a target="_blank" href="{{url('library/'.$book->id.'/downloadbook')}}">{{__('library.show_book')}}</a></p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endif

            </div>
        </div>
    </div>
@endsection

@section('footer')

@endsection