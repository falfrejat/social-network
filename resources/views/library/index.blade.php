@extends('layouts.app')

@section('content')
    <div class="h-20"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                @include('widgets.sidebar')
            </div>
            <div class="col-md-9">

                <div class="content-page-title">
                    <i class="fa fa-empire fa-pdf"></i> {{__('home.library')}}
                </div>
                <div class="content-page-title">
                   <a href="{{url('library/create')}}" class="btn btn-primary">{{__('library.add_book')}}</a>

                    <a href="{{url('library/yourbooks')}}" class="btn btn-primary">{{__('library.your_books')}}</a>
                </div>
                <div class="content-page-blue-title">
                     {{__('library.latest_books')}}
                </div>
                @if($books->count() == 0)
                    <div class="alert-message alert-message-default">
                        <h4>{{__('library.nobook')}}</h4>
                    </div>
                @else
                    <div class="row">
                        @foreach($books as $jey => $book)
                            <div class="col-sm-6 col-md-4">
                                <div class="bs-box" href="{{url('library/book/'.$book->id)}}">
                                    <h3>{{ $book->title }}</h3>
                                    <p> <a target="_blank" href="{{url('library/'.$book->id.'/downloadbook')}}"><i class="fa fa-file-pdf-o"></i> {{__('library.show_book')}}</a></p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endif

            </div>
        </div>
    </div>
@endsection

