@extends('layouts.admin')

@section('content')
    <div class="box-content">
        <h4 class="box-title">{{$title}}</h4>
        <!-- /.box-title -->
        <div class="dropdown js__drop_down">
            <a href="#" class="dropdown-icon glyphicon glyphicon-option-vertical js__drop_down_button"></a>
            <ul class="sub-menu">
                <li><a href="{{route('countries.add')}}">Add Country</a></li>

                <li class="split"></li>

            </ul>
            <!-- /.sub-menu -->
        </div>
        <!-- /.dropdown js__dropdown -->
        @if(Session::has('success'))
        <div class="alert alert-success" role="alert"> <strong>Well done!</strong> {{session('success')}} </div>
        @endif

        <table id="example" class="table table-striped table-bordered display" style="width:100%">
            <thead>
            <tr>
                <th>Name</th>
                <th>Code</th>
                <th>Shortname</th>
                <th>Action</th>
            </tr>
            </thead>
            <tfoot>

            </tfoot>
            <tbody>
            @foreach($countries as $key=>$country)
            <tr>
                <td>{{$country->name}}</td>
                <td>{{$country->phonecode}}</td>
                <td colspan="1">{{$country->iso}}</td>
                <td>
                    <ul class="list-inline">
                        <li class=""><a type="button" href="{{url('admin/countries/edit/'.$country->id)}}" class="btn btn-success btn-xs waves-effect waves-light">Edit</a></li>
                        <li class=""><a  type="button" href="" data-info-id="{{ $country->id }}" class="btn btn-danger btn-xs waves-effect waves-light del-btn">Delete</a></li>
                    </ul>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /.box-content -->
@endsection

@push('footer-script')

    <script>
        $(function() {
            $('body').on('click', '.del-btn', function () {
                var id = $(this).data('info-id');
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this deletion!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: false,
                    closeOnCancel: false,
                    confirmButtonColor: '#f60e0e',
                }, function (isConfirm) {
                    if (isConfirm) {
                        var url = "{{ route('admin.countries.destroy',':id') }}";
                        url = url.replace(':id', id);
                        var token = "{{ csrf_token() }}";
                        $.ajax({
                            type: 'DELETE',
                            url: url,
                            data: {'_token': token},
                            success: function (response) {
                                if (response.status == "success") {
                                    $.unblockUI();
                                    window.location.reload();
                                }
                            }
                        });
                    }else {
						swal({
							title : "Cancelled",
							text: "All is safe :)",
							type: "error",
							confirmButtonColor: '#f60e0e',
						});
					}
                });
                return false;
            });
          //  return false;
        });

    </script>
@endpush