@extends('layouts.admin')

@section('content')
    <div class="col-lg-12 col-xs-12">
        <div class="box-content card white">
            <h4 class="box-title">Add {{$title}}</h4>
            <!-- /.box-title -->
            <div class="card-content">
                <form class="form-horizontal" method="post" action="{{url('admin/countries/store')}}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="inp-type-1" class="col-sm-3 control-label">Name</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="inp-type-1" name="name" placeholder="Name of the country ...">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inp-type-2" class="col-sm-3 control-label">Code</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="inp-type-2" name="code" placeholder="Code">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inp-type-3" class="col-sm-3 control-label">Shortname</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="inp-type-3" name="shortname" placeholder="Shortname" >
                        </div>
                    </div>

                    <div class="form-group margin-bottom-0">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-info btn-sm waves-effect waves-light">Save</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.card-content -->
        </div>
        <!-- /.box-content card white -->
    </div>
@endsection