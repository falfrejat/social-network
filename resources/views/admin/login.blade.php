<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Home</title>
    <link rel="stylesheet" href="{{ asset('admin/assets/styles/style.min.css')}}">

    <!-- Waves Effect -->
    <link rel="stylesheet" href="{{ asset('admin/assets/plugin/waves/waves.min.css')}}">

</head>

<body>

<div id="single-wrapper">
    <form  class="frm-single" method="post" action="{{ route('admin.login') }}">
        {{ csrf_field() }}
        <div class="inside">
            <div class="title"><strong>FXTAMER </strong> Administration Panel</div>
            <!-- /.title -->
            <div class="frm-title">Login</div>
            <!-- /.frm-title -->
            @if ($errors->has('email'))
                <span class="help-block">
                     <strong style="color: red">{{ $errors->first('email') }}</strong>
                </span>
            @endif
            @if ($errors->has('password'))
                <span class="help-block">
                     <strong style="color: red">{{ $errors->first('password') }}</strong>
                </span>
            @endif
            <div class="frm-input"><input name="email" type="text" placeholder="Email" class="frm-inp"><i class="fa fa-user frm-ico"></i></div>
            <!-- /.frm-input -->
            <div class="frm-input"><input name="password" type="password" placeholder="Password" class="frm-inp"><i class="fa fa-lock frm-ico"></i></div>
            <!-- /.frm-input -->
            <div class="clearfix margin-bottom-20">
                <div class="pull-left">
                    <div class="checkbox primary"><input type="checkbox" id="rememberme"><label for="rememberme">Remember me</label></div>
                    <!-- /.checkbox -->
                </div>
                <!-- /.pull-left -->
                <div class="pull-right"><a href="page-recoverpw.html" class="a-link"><i class="fa fa-unlock-alt"></i>Forgot password?</a></div>
                <!-- /.pull-right -->
            </div>
            <!-- /.clearfix -->
            <button type="submit" class="frm-submit">Login<i class="fa fa-arrow-circle-right"></i></button>
            <div class="row small-spacing">
                <div class="col-sm-12">
                    <!-- /.txt-login-with -->
                </div>

            </div>
            <!-- /.row -->

            <div class="frm-footer">BIS © 2021.</div>
            <!-- /.footer -->
        </div>
        <!-- .inside -->
    </form>
    <!-- /.frm-single -->
</div><!--/#single-wrapper -->

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="{{ asset('admin/assets/script/html5shiv.min.js')}}"></script>
<script src="{{ asset('admin/assets/script/respond.min.js')}}"></script>
<![endif]-->
<!--
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="{{ asset('admin/assets/scripts/jquery.min.js')}}"></script>
<script src="{{ asset('admin/assets/scripts/modernizr.min.js')}}"></script>
<script src="{{ asset('admin/assets/plugin/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('admin/assets/plugin/nprogress/nprogress.js')}}"></script>
<script src="{{ asset('admin/assets/plugin/waves/waves.min.js')}}"></script>

<script src="{{ asset('admin/assets/scripts/main.min.js')}}"></script>
</body>
</html>


<!--

    <form name="login-form" class="clearfix" method="POST" action="{{ route('admin.login') }}">
        {{ csrf_field() }}
        <div class="row">
            <div class="form-group col-md-12">
                <label for="form_username_email">Username/Email</label>
                <input id="form_username_email" name="email" class="form-control" type="text">
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-12">
                <label for="form_password">Password</label>
                <input id="form_password" name="password" class="form-control" type="text">
            </div>
        </div>
        <div class="checkbox pull-left mt-15">
            <label for="form_checkbox">
                <input id="form_checkbox" name="form_checkbox" type="checkbox">
                Remember me </label>
        </div>
        <div class="form-group pull-right mt-10">
            <button type="submit" class="btn btn-dark btn-sm">Login</button>
        </div>
        <div class="clear text-center pt-10">
            <a class="text-theme-colored font-weight-600 font-12" href="#">Forgot Your Password?</a>
        </div>
        <div class="clear text-center pt-10">
           <a class="btn btn-dark btn-lg btn-block no-border mt-15 mb-15" href="#" data-bg-color="#3b5998">Login with facebook</a>
             <a clas="btn btn-dark btn-lg btn-block no-border" href="#" data-bg-color="#00acee">Login with twitter</a>
        </div>
    </form>
</div>-->