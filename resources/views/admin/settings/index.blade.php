@extends('layouts.admin')
@push('header-script')
    <!-- Dropify -->
    <link rel="stylesheet" href="{{ asset('admin/assets/plugin/dropify/css/dropify.min.css')}}">
@endpush
@section('content')
    <div class="col-lg-12 col-xs-12">
        <div class="box-content card white">
            <h4 class="box-title"> {{$title}}</h4>
            <!-- /.box-title -->
            <div class="card-content">
                @if(Session::has('alert-success'))
                    <div class="alert alert-success" role="alert"> <strong>Well done!</strong> {{session('alert-success')}} </div>
                @endif
                @if(Session::has('alert-danger'))
                    <div class="alert alert-danger" role="alert"> <strong>Alert !! </strong> {{session('alert-danger')}} </div>
                @endif
                <form class="form-horizontal" method="post" action="{{url('admin/setting/update/')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="inp-type-1" class="col-sm-3 control-label">Title</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="inp-type-1" name="title" value="{{$settings->title}}" placeholder="Name of the country ...">
                            @if ($errors->has('title'))
                                <span class="help-block">
                                     <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inp-type-2" class="col-sm-3 control-label">Title(Arabic)</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="inp-type-2" name="title_ar" value="{{$settings->title_ar}}" >
                            @if ($errors->has('title_ar'))
                                <span class="help-block">
                                     <strong>{{ $errors->first('title_ar') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inp-type-3" class="col-sm-3 control-label">Address</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="inp-type-3" name="address" value="{{$settings->address}}" >
                            @if ($errors->has('address'))
                                <span class="help-block">
                                     <strong>{{ $errors->first('address') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inp-type-1" class="col-sm-3 control-label">Address(Arabic)</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control"  name="address_ar" value="{{$settings->address_ar}}" >
                            @if ($errors->has('address_ar'))
                                <span class="help-block">
                                     <strong>{{ $errors->first('address_ar') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inp-type-1" class="col-sm-3 control-label">Logo</label>
                        <div class="col-xs-9">
                            <div class="box-content">

                                <!-- /.dropdown js__dropdown -->
                                <input type="file" id="input-file-now" name="logo" class="dropify" value="{{ asset('images/'.$settings->logo) }}" />
                            </div>
                            <!-- /.box-content -->
                        </div>
                    </div>


                    <div class="form-group margin-bottom-0">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-info btn-sm waves-effect waves-light">Update</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.card-content -->
        </div>
        <!-- /.box-content card white -->
    </div>
@endsection

@push('footer-script')
<!-- Dropify -->
<script src="{{ asset('admin/assets/plugin/dropify/js/dropify.min.js')}}"></script>
<script src="{{ asset('admin/assets/scripts/fileUpload.demo.min.js')}}"></script>

@endpush