@extends('layouts.admin')
@push('header-script')
    <!-- Data Tables -->
    <link rel="stylesheet" href="{{ asset('admin/assets/plugin/datatables/media/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('admin/assets/plugin/datatables/extensions/Responsive/css/responsive.bootstrap.min.css')}}">

@endpush
@section('content')
    <div class="box-content">
        <h4 class="box-title">{{$title}}</h4>
        <!-- /.box-title -->

        <!-- /.dropdown js__dropdown -->
        @if(Session::has('success'))
        <div class="alert alert-success" role="alert"> <strong>Well done!</strong> {{session('success')}} </div>
        @endif

        <table id="example" class="table table-striped table-bordered display" style="width:100%">
            <thead>
            <tr>
                <th>Post Content</th>
                <th>Owner</th>
                <th>Date Created</th>
                <th>Action</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>Post Content</th>
                <th>Owner</th>
                <th>Date Created</th>
                <th>Action</th>
            </tr>
            </tfoot>
            <tbody>
            @foreach($posts as $post)
            <tr>
                <td>{{$post->content}}</td>
                <td>{{$post->user->name}}</td>
                <td>{{$post->created_at}}</td>

                <td>
                    <ul class="list-inline">
                        @if($post->hidden==0)
                            <li class=""><a type="button" href="{{url('admin/posts/hide/'.$post->id)}}" class="btn btn-xs waves-effect waves-light">Hide</a></li>
                        @else
                            <li class=""><a type="button" href="{{url('admin/posts/unhide/'.$post->id)}}" class="btn btn-xs waves-effect waves-light">Unhide</a></li>
                        @endif
{{--                        <li class=""><a type="button" href="{{url('admin/posts/edit/'.$post->id)}}" class="btn btn-success btn-xs waves-effect waves-light">Edit</a></li>--}}
                        <li class=""><a  type="button" href="" data-info-id="{{ $post->id}}" class="btn btn-danger btn-xs waves-effect waves-light del-btn">Delete</a></li>
                    </ul>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /.box-content -->
@endsection


@push('footer-script')
    <script>
        $(function(){
            $('body').on('click', '.del-btn', function () {
            //$(".del-btn").on("click",function(){
                var id = $(this).data('info-id');
                swal({
                    title: "Reason!",
                    text: "Please, write the reason of deletion:",
                    type: "input",   showCancelButton: true,
                    closeOnConfirm: false,
                    animation: "slide-from-top",
                    confirmButtonColor: '#304ffe',
                }, function(inputValue){
                    if (inputValue === false) return false;
                    if (inputValue === "") {
                        swal({
                            title: "You need to write something!",
                            type: 'error' ,
                            confirmButtonColor: '#f60e0e',
                        });
                        return false
                    }
                    swal({
                        title: "Nice!",
                        text: "You wrote: " + inputValue,
                        type: 'success' ,
                        confirmButtonColor: '#304ffe',
                    });

                    let url = "{{ route('admin.posts.destroy',':id') }}";
                    url = url.replace(':id', id);
                    let token = "{{ csrf_token() }}";
                    $.ajax({
                        type: 'DELETE',
                        url: url,
                        data: {'_token': token,reason:inputValue},
                        success: function (response) {
                            if (response.status == "success") {
                                //$.unblockUI();
                                window.location.reload();
                            }
                        }
                    });

                });
                return false;
            });
            return false;
        });
    </script>

@endpush