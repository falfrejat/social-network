@extends('layouts.admin')
@push('header-script')
    <!-- Data Tables -->
    <link rel="stylesheet" href="{{ asset('admin/assets/plugin/datatables/media/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('admin/assets/plugin/datatables/extensions/Responsive/css/responsive.bootstrap.min.css')}}">

@endpush
@section('content')
    <div class="box-content">
        <h4 class="box-title">{{$title}}</h4>

        <!-- /.dropdown js__dropdown -->
        @if(Session::has('success'))
        <div class="alert alert-success" role="alert"> <strong>Well done!</strong> {{session('success')}} </div>
        @endif

        <table id="example" class="table table-striped table-bordered display" style="width:100%">
            <thead>
            <tr>
                <th>Title</th>
                <th>PDF</th>
                <th>Date Created</th>
                <th>Action</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>Title</th>
                <th>PDF</th>
                <th>Date Created</th>
                <th>Action</th>
            </tr>
            </tfoot>
            <tbody>
            @foreach($books as $book)
            <tr>
                <td>{{$book->title}}</td>
                <td><a href="{{url('admin/library/'.$book->id.'/downloadbook')}}">{{$book->book}}</a></td>
                <td>{{$book->created_at}}</td>
                <td>
                    <ul class="list-inline">
                        <li class=""><a  type="button" href="" data-info-id="{{ $book->id}}" class="btn btn-danger btn-xs waves-effect waves-light del-btn">Delete</a></li>
                    </ul>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /.box-content -->
@endsection


@push('footer-script')
    <script>
        $(function() {
            $('body').on('click', '.del-btn', function () {
                var id = $(this).data('info-id');
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this deletion!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: false,
                    closeOnCancel: false,
                    confirmButtonColor: '#f60e0e',
                }, function (isConfirm) {
                    if (isConfirm) {
                        var url = "{{ route('admin.jobs.destroy',':id') }}";
                        url = url.replace(':id', id);
                        var token = "{{ csrf_token() }}";
                        $.ajax({
                            type: 'DELETE',
                            url: url,
                            data: {'_token': token},
                            success: function (response) {
                                if (response.status == "success") {
                                    $.unblockUI();
                                    window.location.reload();
                                }
                            }
                        });
                    }else {
						swal({
							title : "Cancelled",
							text: "All is safe :)",
							type: "error",
							confirmButtonColor: '#f60e0e',
						});
					}
                });
                return false;
            });
          //  return false;
        });

    </script>
@endpush