@extends('layouts.admin')

@section('content')
    <div class="box-content">
        <h4 class="box-title">Users</h4>
        <!-- /.box-title -->
        <div class="dropdown js__drop_down">
            <a href="#" class="dropdown-icon glyphicon glyphicon-option-vertical js__drop_down_button"></a>
            <ul class="sub-menu">
                <li><a href="{{route('users.add')}}">Add user</a></li>

                <li class="split"></li>

            </ul>
            <!-- /.sub-menu -->
        </div>
        <!-- /.dropdown js__dropdown -->
        <table id="example" class="table table-striped table-bordered display" style="width:100%">
            <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Birthday</th>
                <th>Gender</th>
                <th>Phone</th>
                <th>Action</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Birthday</th>
                <th>Gender</th>
                <th>Phone</th>
                <th>Action</th>
            </tr>
            </tfoot>
            <tbody>
            @foreach($users as $key=>$user)
            <tr>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                @php  $pieces = explode(" ", $user->birthday); @endphp
                <td>{{$pieces[0]}}</td>
                <td>{{($user->sex==0)?'Male':'Female'}}</td>
                <td>{{$user->phone}}</td>
                <td>
                    <ul class="list-inline">
                        @if($user->active==1)
                        <li class=""><a type="button" href="{{url('admin/users/deactivate/'.$user->id)}}" class="btn btn-xs waves-effect waves-light">Deactivate</a></li>
                        @else
                        <li class=""><a type="button" href="{{url('admin/users/activate/'.$user->id)}}" class="btn btn-xs waves-effect waves-light">Activate</a></li>
                        @endif
                        <li class=""><a type="button" data-info-id="{{ $user->id }}"  class="btn btn-danger btn-xs waves-effect waves-light del-btn">Delete</a></li>
                    </ul>
                </td>
            </tr>
         @endforeach
            </tbody>
        </table>
    </div>
    <!-- /.box-content -->
@endsection

@push('footer-script')

    <script>
        $(function() {
            $('body').on('click', '.del-btn', function () {
                var id = $(this).data('info-id');
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this deletion!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: false,
                    closeOnCancel: false,
                    confirmButtonColor: '#f60e0e',
                }, function (isConfirm) {
                    if (isConfirm) {
                        var url = "{{ url('admin/users/destroy') }}";
                        //url = url.replace(':id', id);
                        var token = "{{ csrf_token() }}";
                        $.ajax({
                            type: 'DELETE',
                            url: url,
                            data: {'_token': token,id:id},
                            success: function (response) {
                                if (response.status == "success") {
                                    $.unblockUI();
                                    window.location.reload();
                                }
                            }
                        });
                    }else {
                        swal({
                            title : "Cancelled",
                            text: "All is safe :)",
                            type: "error",
                            confirmButtonColor: '#f60e0e',
                        });
                    }
                });
                return false;
            });
        });
    </script>
@endpush