<?php

return [

    'name' => 'اسم المجموعة',
    'create' => 'أنشىء',
    'title' => 'المجموعات',
    'your_groups' => 'مجموعاتك',
    'no_group'  => 'لايوجد لديك مجموعات',
	'create_group'  => 'أنشئ مجموعة',
    'you_not_in_group' => 'أنت لست مشترك في أي مجموعة',

    /*page*/
    'pages_title' => 'الصفحات',
    'create_page' => 'إنشاء صفحة',
    'page_name' => 'اسم الصفحة',
    'profile_page' => 'صورة الصفحة',
    'description'=> 'الوصف',
    'you_dont_have_pages' => 'ليس لديك صفحات',

    /*saved posts*/
    'your_saved_posts' => 'منشوراتك المحفوظة',
    'no_saved_posts' => 'لايوجد لديك منشورات محفوظة',
    'invite' => 'دعوة الأصدقاء',
    'invite_others' => 'دعوة الآخرين'


];