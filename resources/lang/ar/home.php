<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'home' => 'الرئيسية',
    'lang' => 'اللغة',
    'groups' => 'المجموعات',
    'jobs' => 'العمل',
    'suggested_poeple' => 'أشخاص مقترحين',
    'add_image' => 'إضافة صورة',
    'add_image_video' => 'إضافة صورة أو فيديو',
    'add_doc' => 'إضافة ملف',
    'post' => 'انشر!',
    'likes' => 'الإعجابات',
    'likesNum' => 'إعجابات',
    'likeNum' => 'إعجاب',
    'no_comment' => 'لايوجد تعليقات! اترك تعليقاً',
    'comments' => ' تعليقات ',
    'my_profile' => 'صفحتي الشخصية',
    'settings' => 'الإعدادات',
    'logout' => 'تسجيل الخروج',
    /*profile*/
    'edit' => 'تعديل',
    'delete' => 'حذف',

    'gender' => 'الجنس',
    'birthday' => 'تاريخ الميلاد',
    'male' => 'ذكر',
    'female' => 'أنثى',
    'your_information' => 'معلوماتك الشخصية',
    'phone' => 'هاتف',
    /*settings*/
    'account_settings' =>'تعديلات الحساب',
    'name'  => 'الاسم',
    'email_address'  => 'البريد الالتكروني',
    'private_account'  => 'حساب خاص',
    'update' => 'تعديل',
    'username_settings' =>'تعديل اسم المستخدم',
    'username' => 'اسم المستخدم',
    'password_settings' =>'تعديل كلمة السر',
    'current_password' => 'كلمة السر الحالية',
    'new_password' => 'كلمة السر الجديدة',
    'confirm_password' => 'تأكيد كلمة السر',

    'posts' => 'المنشورات',
    'following' => 'تتابع',
    'followers' => 'المتابعون',

    'add_comment' => 'أضف تعليق',
    'follow' => 'متابعة',

    'country' => 'الدولة',
    'city' => 'المدينة',

    'chooseCountry' => 'اختر دولة',
    'create_job' => 'إنشاء فرصة عمل',
	
	'save' => 'حفظ',
    'close' => 'إغلاق',
    'ads'   => 'إعلانات',

    'cancel' => 'إلغاء',
    'market_places' => 'أماكن التسوق',
    'library'   => 'المكتبة',
    'forgot_password' => 'نسيت كلمة السر؟',
    'pages'=> 'الصفحات',
    'save_post' => 'حفظ المنشور',
    'post_saved' => ' تم الحفظ',
    'message' => 'الرسالة:',
    'savedposts' => 'المنشورات المحفوظة',
    'hide' => 'إخفاء',
    'file' => 'ملف أو صورة',



];