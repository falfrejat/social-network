<?php

return [

    'add_book' => 'أضف كتاب',
    'nobook' => 'لايوجد كتب',
    'category' => 'التصنيف ',
    'latest_books' => 'آخر الكتب',
    'your_books' => 'كتب أدخلتها',
    'show_book' => 'عرض',
    'show_doc' => 'مشاهدة الملف',
    'add' => 'أضف',
    'pdffile' => 'حمل الكتاب'
];