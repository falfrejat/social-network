<?php

return [

    'content' => 'Content',
    'add_job' => 'Add Job Opportunity',
    'nojobs' => 'There is no job',
    'title' => 'Title',
    'company' => 'Company',
    'your_jobs' => 'Your Jobs Oppurtunities',
    'latest_jobs' => 'Latests Jobs Oppurtunities',
    'details' => 'Details',
    'tags'  => 'Tags',
    'writetag'  => 'Write a tag then press out side the box and repeat',
    /*work experience*/
    'work_experience' => 'Work Experience',
    'your_cv'        => 'Your CV',
    'last_work' => 'Last Work',
    'download_cv' => 'Download CV',
    /* apply job*/
    'apply_job' => 'Apply This Job',
    'applying_job' => 'Applying the Job',
    'send' => 'Send',
    'fill_information' => 'Fill your Informations',

    'full_name' => 'Full Name',
    'email' => 'E-mail',
    'phone' => 'Phone',
    'upload_your_cv' => 'Upload Resume',
    'cv' => 'CV',
    'who_apply' => 'Who Applied this Job',
    'downloadCV' => 'Download CV',


];