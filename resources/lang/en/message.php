<?php

return [
        /*New group*/
    'group_added' => 'New group have been successfully Added!!',
    'group_not_added' =>'There was a problem saving your settings!!',

     'job_added' => 'New job oppurtunity have been successfully Added!!',
    'job_not_added' =>'There was a problem saving your job!!',

     'ad_added' => 'New ad  have been successfully Added!!',
    'ad_not_added' =>'There was a problem saving your ad!!',

    'settings_edited' => 'The settings of the websites were update!!',
    'settings_not_edited' => 'The settings of the websites were NOT update!!',

    'book_added' => 'New Book have been successfully Added!!',
    'book_not_added' =>'There was a problem saving your Book!!',

    'you_applied_job' => 'You applied successfully to this Job',
    'try_again' => 'Try Again later, please!',
    'some_thing_wrong' => 'Some thing went wrong, Try again Later',

    'page_added' => 'New Page have been successfully Added!!',
    'page_not_added' =>'There was a problem saving your Page!!',

    ];