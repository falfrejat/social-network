<?php

return [

    'add_book' => 'Add book',
    'nobook' => 'No books',

    'category' => 'Category',
    'latest_books' => 'Latest Books',
    'your_books' => 'Your Books',
    'show_book' => 'View Book',
    'show_doc' => 'View Doc',
    'add' => 'Add',
    'pdffile' => 'Upload Book',




];