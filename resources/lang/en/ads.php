<?php

return [

    'ads' => 'Ads',
    'noads' => 'No Ads',
    'create_ad' => 'Add a New Ad',

    'content' => 'Content',
    'title' => 'Title',

    'image' => 'Upload Image',
    'latest_ads' => 'Latest Ads',
    'price' => 'Price',
    'category' => 'Category'



];