<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'home' => 'Home',
    'lang' => 'Lang',
    'groups' => 'Groups',
    'jobs' => 'Jobs',
    'suggested_poeple' => 'Suggested People',
    'add_image' => 'Add Image',
    'add_image_video' => 'Add Image or Video',
    'add_doc' => 'Add Document',
    'post' => 'Post!',
    'likes' => 'Likes',
    'likesNum' => 'likes',
    'likeNum' => 'like',
    'no_comment' => 'No Comments! Write a comment',
    'comments' => ' comments',
    'my_profile' => 'My Profile',
    'settings' => 'Settings',
    'logout' => 'Logout',
    /*profile*/
    'edit' => 'Edit',
    'delete' => 'Delete',
    'gender' => 'Gender',
    'birthday' => 'Birthday',
    'male' => 'Male',
    'female' => 'Female',
    'your_information' => 'Your Information',
    'phone' => 'Phone',
    /*settings*/
    'account_settings' =>'Account Settings',
    'name'  => 'Name',
    'email_address'  => 'E-Mail Address',
    'private_account'  => 'Private Account',
    'update' => 'Update',
    'username_settings' =>'Username Settings',
    'username' => 'Username',
    'password_settings' =>'Password Settings',
    'current_password' => 'Current Password',
    'new_password' => 'New Password',
    'confirm_password' => 'Confirm New Password',

    'posts' => 'POSTS',
    'following' => 'FOLLOWING',
    'followers' => 'FOLLOWERS',

    'add_comment' => 'Submit!',

    'follow' => 'Follow',
    'country' => 'Country',
    'city' => 'City',

    'chooseCountry'=>'Choose Country',
    'create_job' => 'Add a Job Opportunity',

	 'save' => 'Save',
    'close' => 'Close',
    'ads'   => 'Ads',

    'cancel' => 'Cancel',
    'market_places' => 'Market Places',
    'library'   => 'Library',
    'forgot_password' => 'Forgot Your Password?',
    'pages'=> 'Pages',
    'save_post' => 'Save Post',
    'post_saved' => 'Post Saved',
    'message' => 'Message:',
    'savedposts' => 'Saved posts',
    'hide' => 'Hide',
    'file' => 'File',

];