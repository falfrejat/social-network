<?php

return [
    /*create group*/
    'name' => 'Group Name',
    'create' => 'Create',
    'title' => 'Groups',
    'your_groups' => 'Your Groups',
    'no_group'  => 'No Group!',
	'create_group'  => 'Create a Group',
    'you_not_in_group' => 'You are not in any group.',


    /*page*/
    'pages_title' => 'Pages',
    'create_page' => 'Create Page',
    'page_name' => 'Page Name',
    'profile_page' => 'Page Image',

    'description'=> 'Description',
    'you_dont_have_pages' => 'You don\'t have pages.',
    
    /*saved posts*/
    'your_saved_posts' => 'Your Saved Posts',
    'no_saved_posts' => 'You don\'t have any saved posts.',
    'invite' => 'Invite ',
    'invite_others' => 'Invite Others'


];