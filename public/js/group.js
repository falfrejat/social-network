/**
 * Created by lvntayn on 03/06/2017.
 */



// invite to group
function invite(){
    var form_name = '#form-groupInvite';

    $('.loading-page').show();

    var data = new FormData();
    data.append('information', JSON.stringify(makeSerializable(form_name).serializeJSON()));
    var invited = $('#invited').val();
    data.append('invited', invited);
    //console.log(invited);
    $.ajax({
        url: BASE_URL+'/group/invite',
        type: "POST",
        timeout: 5000,
        data: data,
        contentType: false,
        cache: false,
        processData: false,
        headers: {'X-CSRF-TOKEN': CSRF},
        success: function(response){
            if (response.code == 200){
                window.location.reload(true);
            }else{
                $('#errorMessageModal').modal('show');
                $('#errorMessageModal #errors').html(response.message);
                $('.loading-page').hide();
            }
        },
        error: function(){
            $('#errorMessageModal').modal('show');
            $('#errorMessageModal #errors').html('Something went wrong!');
            $('.loading-page').hide();
        }
    });

}



