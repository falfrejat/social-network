<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ads extends Model
{
    protected $table = 'ads';

    //public $timestamps = false;
    protected $fillable = [
        'title','text','image','user_id','price','category_id'
    ];

    public function user(){
        return $this->belongsTo('App\Models\User', 'user_id');
    }
    public function category(){
        return $this->belongsTo('App\Models\AdsCategory', 'category_id');
    }
}
