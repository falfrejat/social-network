<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Library extends Model
{
    protected $table = 'libraries';

    protected $fillable = [
        'title','text','image','user_id','price','category_id'
    ];

    public function user(){
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
