<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorkExp extends Model
{
    protected $table = 'user_cv';

    public $timestamps = false;
    protected $fillable = [
        'cv','workexpereince','user_id'
    ];
}
