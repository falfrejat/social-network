<?php
/**
 * Created by lvntayn
 * Date: 04/06/2017
 * Time: 17:23
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class PostImage extends Model
{

    protected $table = 'post_images';

    public $timestamps = false;


    public function getURL(){
        //return url('storage/uploads/posts/'.$this->image_path);storage/app/public
        return asset('storage/uploads/posts/'.$this->image_path);
        //return asset('storage/app/public/uploads/posts/'.$this->image_path);
    }

    public function getURLVideo(){
        //return url('/uploads/posts/'.$this->image_path);
         return asset('storage/uploads/posts/'.$this->image_path); //....... local
        //return asset('storage/app/public/uploads/posts/'.$this->image_path);
    }
    public function getURLDoc(){
        //return url('/uploads/posts/'.$this->image_path);
        return asset('storage/uploads/posts/'.$this->image_path); //....... local
        //return asset('storage/app/public/uploads/posts/'.$this->image_path);
    }


}