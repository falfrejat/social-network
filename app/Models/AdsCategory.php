<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdsCategory extends Model
{
    protected $table = 'ads_category';

    //public $timestamps = false;
    protected $fillable = [
        'name'
    ];

}
