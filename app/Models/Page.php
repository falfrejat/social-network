<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Page extends Model
{


    use Notifiable;

    public function getPhoto($w = null, $h = null){
        if (!empty($this->profile_path)){
            $path = url('storage/uploads/page/profile/'.$this->profile_path);
        }else {
            $path = url('images/profile-picture.png');
        }
        if ($w == null && $h == null){
            return url('/'.$path);
        }
        $image = '/resizer.php?';
        if ($w > -1) $image .= 'w='.$w;
        if ($h > -1) $image .= '&h='.$h;
        $image .= '&zc=1';
        $image .= '&src='.$path;
        return url($image);
    }
}
