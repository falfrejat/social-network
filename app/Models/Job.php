<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $table = 'user_jobs';

    public $timestamps = true;
    protected $fillable = [
       'title', 'text','company','user_id','tags'
    ];

    public function user(){
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
