<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\WorkExp;
use App\Models\Job;
use Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

class WorkExpController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function downloadCV($workExpId)
    {
        $workExpForUser = WorkExp::find($workExpId);
        $path = public_path('storage/uploads/cv/'.$workExpForUser->cv);
        return response()->file($path);
    }
    
    public function update(Request $request, $workExpId)
    {
        $workExp  = WorkExp::find($workExpId);
        $workExp->workexpereince = $request->input('workexpereince');

        $messages = [
            'image.mimes' => trans('validation.mimes'),
            'image.max.file' => trans('validation.max.file'),
        ];

        $validator = Validator::make(array('cv' => $request->file('cv')), [
            'cv' => 'mimes:pdf|max:2048'
        ], $messages);
        if($request->hasFile('cv'))
        {
            if ($validator->fails()) {
                $response['code'] = 400;
                $response['message'] = implode(' ', $validator->errors()->all());
            }else{
                $file = $request->file('cv');
                $file_name = md5(uniqid() . time()) . '.' . $file->getClientOriginalExtension();
                if ($file->move('storage/uploads/cv/', $file_name)) {
                    $workExp->cv = $file_name;
                    $workExp->update();
                    return redirect('/'.Auth::user()->username);
                }else{
                    return redirect('/'.Auth::user()->username);
                }
            }
        }
        else
            {
                $workExp->update();
                return redirect('/'.Auth::user()->username);
            }
       

//
//        if ($workExp->save()){
//            $request->session()->flash('alert-success', __('message.job_added'));
//            return redirect('jobs/create');
//        }else{
//            $request->session()->flash('alert-danger', __('message.job_not_added'));
//            return redirect('jobs/create');
//        }
    }
}
