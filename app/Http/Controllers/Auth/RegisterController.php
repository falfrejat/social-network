<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Models\WorkExp;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:191',
            'email' => 'required|string|email|max:191|unique:users',
            'username' => 'required|max:191|unique:users,username|usernamecheck',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $confirmation_code = str_random(30);
        $email = $data['email'];
        $username = $data['username'];
         $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'username' => $data['username'],
            'password' => bcrypt($data['password']),
            'confirmation_code' => $confirmation_code,
            'sex' => $data['sex']
        ]);

         $we  = WorkExp::create([
            'user_id' => $user->id,
            'workexpereince'=>'',
            'cv' => ''
         ]);

         //if local don't send email
         //if (!(strpos($_SERVER['SERVER_NAME'], 'localhost') !== false)) {
             Mail::send('email.verify', array('confirmation_code' => $confirmation_code), function ($message) use ($email, $username) {

                 $message
                     ->to($email, $username)
                     ->subject('Verify your Email Address');
             });
         //}

        Session::flash('message','Thanks for signing up! Please check your email.');
        return $user;

    }

    public function confirm($confirmation_code)
    {
        if( ! $confirmation_code)
        {
            throw new InvalidConfirmationCodeException;
        }

        $user = User::where('confirmation_code',$confirmation_code)->first();

        if ( ! $user)
        {
            throw new InvalidConfirmationCodeException;
        }

        $user->confirmed = 1;
        $user->confirmation_code = null;
        $user->save();

        Session::flash('message','You have successfully verified your account.');
        return redirect()->route('login');
    }

}
