<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Library\Reply;

class AdminCityController extends Controller
{
    public function __construct()
    {
        //parent::__construct();
        $this->pageTitle = "City";
        //$this->pageIcon = 'icon-user';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = $this->pageTitle;
        $cities = City::with('country')->get();
       // DB::table('roles')->pluck('title');
        //var_dump($cities);
        return view('admin.cities.index', compact('cities','title'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = $this->pageTitle;
        $countries = Country::all();
        return view('admin.cities.create', compact('title','countries'));
    }
    /**
     * @param StoreTeam $request
     * @return array
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'country_id' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('admin/cities/add')
                ->withErrors($validator)
                ->withInput();
        }

        $city = new City();
        $city->name = $request->name;
        $city->country_id = $request->country_id;

        if($city->save())
        {
            $request->session()->flash('success', __('New City Added Successfully'));
            return redirect('admin/cities');
        }else{
            return redirect('admin/cities')
                ->withErrors($validator);
        }
    }
    /**
     * Display the specified resource.
     *[
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
//        $this->adjustment = Adjustment::with('warehouse', 'items')->findOrFail($id);
//        return view('admin.adjustment.view', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = $this->pageTitle;
        $city = City::findORfail($id);
        $countries = Country::all();
        //$this->adjustment = Adjustment::with('warehouse', 'items')->findOrFail($id);
        return view('admin.cities.edit', compact('title','city', 'countries'));
    }

    /**
     * @param StoreTeam $request
     * @param $id
     * @return array
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',

        ]);

        if ($validator->fails()) {
            return redirect('admin/cities/edit/'.$id)
                ->withErrors($validator)
                ->withInput();
        }
        $city = City::findORfail($id);
        $data =  $request->except(['_method', '_token']);
        //$country->update($data)
        //return Reply::redirect(route('admin.country.index'), __('adjustment Updated'));
        if( $city->update($data))
        {
            $request->session()->flash('success', __('City Updated Successfully'));
            return redirect('admin/cities');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        City::destroy($id);
        return Reply::dataOnly(['status' => 'success']);
    }

}