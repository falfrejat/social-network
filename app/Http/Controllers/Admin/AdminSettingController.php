<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Library\Reply;

class AdminSettingController extends Controller
{
    public function __construct()
    {
        //parent::__construct();
        $this->pageTitle = "Settings";
        //$this->pageIcon = 'icon-user';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = $this->pageTitle;
        $settings = Setting::findOrfail(1);
        return view('admin.settings.index', compact('settings','title'));
    }


    public function show($id)
    {
//        $title = $this->pageTitle;
//        $settings = Setting::findOrfail(1);
//        return view('admin.setting.index', compact('settings','title'));
    }

    /**
     * @param StoreTeam $request
     * @param $id
     * @return array
     */
    public function update(Request $request)
    {
        $setting = Setting::findORfail(1);

        if($request->hasFile('logo'))
        {
            $file = $request->file('logo');
            $file_name = md5(uniqid() . time()) . '.' . $file->getClientOriginalExtension();
            if ($file->move('images/', $file_name)) {
                $setting->logo = $file_name;
            }
            $setting->title = $request->input('title');
            $setting->title_ar = $request->input('title_ar');
            $setting->address =  $request->input('address');
            $setting->address_ar = $request->input('address_ar');
            $setting->phone = $request->input('phone');
            $setting->mobile = $request->input('mobile');

            //$setting->save();
            if ($setting->save()){
                $request->session()->flash('alert-success', __('message.ad_added'));
                return redirect('admin/setting');
            }else{
                $request->session()->flash('alert-danger', __('message.ad_not_added'));
                return redirect('admin/setting');
            }
        }
        else {
            $setting->title = $request->input('title');
            $setting->title_ar = $request->input('title_ar');
            $setting->address = $request->input('address');
            $setting->address_ar = $request->input('address_ar');
            $setting->phone = $request->input('phone');
            $setting->mobile = $request->input('mobile');

            //$setting->save();
            if ($setting->save()) {
                $request->session()->flash('alert-success', __('message.settings_edited'));
                return redirect('admin/setting');
            } else {
                $request->session()->flash('alert-danger', __('message.settings_not_edited'));
                return redirect('admin/setting');
            }
        }
        //return Reply::redirect(route('admin.country.index'), __('adjustment Updated'));
//        if( $settings->update($data))
//        {
//            $request->session()->flash('success', __('Country Updated Successfully'));
//            return redirect('admin/countries');
//        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //EmployeeDetails::where('department_id', $id)->update(['department_id' => NULL]);
        //JobApplication::where('department', $id)->update(['department' => NULL]);
        //Designation::where('department_id', $id)->update(['department_id' => 0]);
        Country::destroy($id);
        return Reply::dataOnly(['status' => 'success']);
    }

}