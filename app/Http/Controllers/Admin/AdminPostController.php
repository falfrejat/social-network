<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Log;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Library\Reply;

class AdminPostController extends Controller
{
    public function __construct()
    {
        //parent::__construct();
        $this->pageTitle = "Posts";
        //$this->pageIcon = 'icon-user';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = $this->pageTitle;
        $posts = Post::all();

        return view('admin.posts.index', compact('posts','title'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//        $title = $this->pageTitle;
//        $countries = Country::all();
//        return view('admin.cities.create', compact('title','countries'));
    }
    /**
     * @param StoreTeam $request
     * @return array
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'country_id' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('admin/cities/add')
                ->withErrors($validator)
                ->withInput();
        }

        $city = new City();
        $city->name = $request->name;
        $city->country_id = $request->country_id;

        if($city->save())
        {
            $request->session()->flash('success', __('New City Added Successfully'));
            return redirect('admin/cities');
        }else{
            return redirect('admin/cities')
                ->withErrors($validator);
        }
    }
    /**
     * Display the specified resource.
     *[
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
//        $this->adjustment = Adjustment::with('warehouse', 'items')->findOrFail($id);
//        return view('admin.adjustment.view', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
//        $title = $this->pageTitle;
//        $city = City::findORfail($id);
//        $countries = Country::all();
//        //$this->adjustment = Adjustment::with('warehouse', 'items')->findOrFail($id);
//        return view('admin.cities.edit', compact('title','city', 'countries'));
    }
    /**
     * @param StoreTeam $request
     * @param $id
     * @return array
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect('admin/cities/edit/'.$id)
                ->withErrors($validator)
                ->withInput();
        }
        $city = City::findORfail($id);
        $data =  $request->except(['_method', '_token']);
        if( $city->update($data))
        {
            $request->session()->flash('success', __('City Updated Successfully'));
            return redirect('admin/cities');
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reason = request()->get('reason');
        $post = Post::find($id);
        $log = new Log();
        $log->reason = $reason;
        $log->action = 'Delete a post';
        $log->content = $post->content;
        $log->deleted_id = $id;
        $log->save();

        Post::destroy($id);
        return Reply::dataOnly(['status' => 'success']);
    }

    //Hide post
    public function hide($id)
    {
        $post = Post::findOrFail($id);
        //var_dump($post);
        $data = array('hidden' => 1);

        $post->update($data);
        return redirect('admin/posts');
    }
    //Unhide post
    public function unhide($id)
    {
        $post = Post::findOrFail($id);
        $data = array('hidden' => 0);
        $post->update($data);
        return redirect('admin/posts');
    }

}