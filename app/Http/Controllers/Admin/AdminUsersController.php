<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Library\Reply;
//use App\Models\Country;
use App\Models\User;


class AdminUsersController extends Controller
{
    public function __construct()
    {
        //parent::__construct();
        $this->pageTitle = "Users";
        //$this->pageIcon = 'icon-user';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$this->users = User::with('warehouse', 'items')->get();
        $users = User::all();
        return view('admin.users.index', compact('users'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->inventories = InventoryDetails::all();
        $this->items = Items::all();
        return view('admin.adjustment.create', $this->data);
    }
    /**
     * @param StoreTeam $request
     * @return array
     */
    public function store(StoreAdjustmentRequest $request)
    {
    }
    /**
     * Display the specified resource.
     *[
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * @param StoreTeam $request
     * @param $id
     * @return array
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        $id = request()->get('id');
        User::destroy($id);
        return Reply::dataOnly(['status' => 'success']);
    }

    //Deactivate user
    public function deactivate($id)
    {
        $user = User::findOrFail($id);
        //$data =  $request->except(['_method', '_token']);
        $data = array('active' => 0);
        $user->update($data);
        return redirect('admin/users');
    }
    //Activate user
    public function activate($id)
    {
        $user = User::findOrFail($id);
        //$data =  $request->except(['_method', '_token']);
        //var_dump($user);
        $data = array('active' => 1);
        $user->update($data);
        return redirect('admin/users');
    }
}