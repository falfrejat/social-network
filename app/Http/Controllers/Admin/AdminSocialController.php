<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Social;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Library\Reply;

class AdminSocialController extends Controller
{
    public function __construct()
    {
        //parent::__construct();
        $this->pageTitle = "Social";
        //$this->pageIcon = 'icon-user';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$this->users = User::with('warehouse', 'items')->get();
        $title = $this->pageTitle;
        $social = Social::findOrfail(1);
        return view('admin.social.index', compact('social','title'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = $this->pageTitle;
        return view('admin.countries.create', compact('title'));
    }
    /**
     * @param StoreTeam $request
     * @return array
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        $country = new Country();

        $country->name = $request->name;
        $country->code = $request->code;
        $country->shortname = $request->shortname;
        if($country->save())
        {
            $request->session()->flash('success', __('Country Added Successfully'));
        }

        if($country->save())
        {
            return redirect('admin/countries');
        }else{
            return redirect('admin/countries')
                ->withErrors($validator);
        }
    }
    /**
     * Display the specified resource.
     *[
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->adjustment = Adjustment::with('warehouse', 'items')->findOrFail($id);
        return view('admin.adjustment.view', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = $this->pageTitle;
        $country = Country::findORfail($id);
        //$this->adjustment = Adjustment::with('warehouse', 'items')->findOrFail($id);
        return view('admin.countries.edit', compact('title','country'));
    }

    /**
     * @param StoreTeam $request
     * @param $id
     * @return array
     */
    public function update(Request $request, $id)
    {
        //var_dump($id);
        $country = Country::findORfail($id);
        $data =  $request->except(['_method', '_token']);
        //$country->update($data)
        //return Reply::redirect(route('admin.country.index'), __('adjustment Updated'));
        if( $country->update($data))
        {
            $request->session()->flash('success', __('Country Updated Successfully'));
            return redirect('admin/countries');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //EmployeeDetails::where('department_id', $id)->update(['department_id' => NULL]);
        //JobApplication::where('department', $id)->update(['department' => NULL]);
        //Designation::where('department_id', $id)->update(['department_id' => 0]);
        Country::destroy($id);
        return Reply::dataOnly(['status' => 'success']);
    }

}