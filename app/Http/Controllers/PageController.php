<?php

namespace App\Http\Controllers;

use App\Models\Post as Post;
use App\Models\Setting;
use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use View;
class PageController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $setting = Setting::find(1);
        View::share('setting',$setting);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $pages = Page::join('users', 'users.id', '=', 'pages.owner_id')
            ->where('owner_id', $user->id)->select('pages.*');

        $city = $user->location->city;

        return view('pages.index', compact('user', 'pages','city'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        return view('pages.create',compact('user'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // var_dump($request->all());;
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        $name = $request->input('name');
        $description = $request->input('description');

        if ($validator->fails()) {
            return redirect('pages/create')
                ->withErrors($validator)
                ->withInput();
        }
        $page = new Page();
        if($request->hasFile('profile_path'))
        {
            $file = $request->file('profile_path');
            $file_name = md5(uniqid() . time()) . '.' . $file->getClientOriginalExtension();
            if ($file->move('storage/uploads/page/profile/', $file_name)) {
                $page->profile_path = $file_name;
            }
            $page->name = $request->input('name');
            $page->description = $request->input('description');

            $user_id = Auth::user()->id;
            $page->owner_id = $user_id;
            if ($page->save()){
                $request->session()->flash('alert-success', __('message.page_added'));
                return redirect('pages/create');
            }else{
                $request->session()->flash('alert-danger', __('message.page_not_added'));
                return redirect('pages/create');
            }
        }
        else
        {

        }
    }

    public function page($id){

        if(!is_numeric($id)) return redirect('/404');
        $user = Auth::user();
        //$group = $this->group;
        $page = Page::find($id);
        //var_dump($page);
        $wall = [
            'new_post_group_id' => 0,
            'new_post_page_id' => $page->id
        ];

        $city = $user->location->city;
        return view('pages.page', compact('user', 'page', 'wall', 'city'));
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        //
    }
}
