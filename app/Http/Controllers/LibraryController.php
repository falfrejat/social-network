<?php

namespace App\Http\Controllers;

use App\Models\Ads;
use App\Models\Group;
use App\Models\Job;
use App\Models\Library;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Response;

class LibraryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $setting = Setting::find(1);
        View::share('setting',$setting);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $groups = Group::join('user_hobbies', 'user_hobbies.hobby_id', '=', 'groups.hobby_id')
            ->where('user_hobbies.user_id', $user->id)->select('groups.*');

        $books = Library::orderBy('id', 'desc')->take(10)->get();
        return view('library.index', compact('user','groups','books'));
    }

    public function downloadBook($bookId)
    {
        $book = Library::find($bookId);
        $path = public_path('storage/uploads/library/'.$book->book);
        //return response()->file($path);
        return Response::make(file_get_contents($path), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="'.$book.'"'
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        return view('library.create',compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //var_dump($request->all());
        $messages = [
            'book.mimes' => trans('validation.mimes'),
            'book.max.file' => trans('validation.max.file'),
        ];
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'image' => 'mimes:pdf|max:2048'
        ],$messages);

        if ($validator->fails()) {
            return redirect('library/create')
                ->withErrors($validator)
                ->withInput();
        }
        $library = new Library();
        if($request->hasFile('book'))
        {
            $file = $request->file('book');
            $file_name = md5(uniqid() . time()) . '.' . $file->getClientOriginalExtension();
            if ($file->move('storage/uploads/library/', $file_name)) {
                $library->book = $file_name;
            }
            $library->title = $request->input('title');
            $user_id = Auth::user()->id;
            $library->user_id = $user_id;
            if ( $library->save()){
                $request->session()->flash('alert-success', __('message.book_added'));
                return redirect('library/create');
            }else{
                $request->session()->flash('alert-danger', __('message.book_not_added'));
                return redirect('library/create');
            }
        }
    }

    public function yourbooks()
    {
        $user = Auth::user();
        $books = Library::where('user_id', $user->id)->orderBy('id', 'desc')->take(10)->get();
        return view('library.yourbooks',compact('books','user'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Library  $library
     * @return \Illuminate\Http\Response
     */
    public function book($id)
    {
        $user = Auth::user();
        $book = Library::find($id);
        return view('library.book',compact('book','user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Library  $library
     * @return \Illuminate\Http\Response
     */
    public function edit(Library $library)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Library  $library
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Library $library)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Library  $library
     * @return \Illuminate\Http\Response
     */
    public function destroy(Library $library)
    {
        //
    }
}
