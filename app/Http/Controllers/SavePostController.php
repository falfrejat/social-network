<?php

namespace App\Http\Controllers;

use App\Models\SavePost;
use App\Models\Setting;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

//use App\Http\Controllers\Auth;
class SavePostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $setting = Setting::find(1);
        View::share('setting',$setting);
    }
    public function index()
    {
        $user = Auth::user();
        $saved_posts = SavePost::where('user_id',$user->id);
        return view('saved-post.index', compact('saved_posts','user'));
        //var_dump($saved_posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SavePost  $savePost
     * @return \Illuminate\Http\Response
     */
    public function show(SavePost $savePost)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SavePost  $savePost
     * @return \Illuminate\Http\Response
     */
    public function edit(SavePost $savePost)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SavePost  $savePost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SavePost $savePost)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SavePost  $savePost
     * @return \Illuminate\Http\Response
     */
    public function destroy(SavePost $savePost)
    {
        //
    }
}
