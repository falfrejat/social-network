<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Group;
use App\Models\Post as Post;
use App\Models\Hobby;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use View;
use Response;

class GroupController extends Controller
{

    public $group;
    public function __construct()
    {
        $this->middleware('auth');
        $setting = Setting::find(1);
        View::share('setting',$setting);
    }

    public function secure($id){
        $group = Group::find($id);

        if ($group){
            $this->group = $group;

            if (!Auth::user()->hasHobby($this->group->hobby_id)) return false;

            return true;
        }
        return false;
    }

    public function index()
    {
        $user = Auth::user();
        $groups = Group::join('user_hobbies', 'user_hobbies.hobby_id', '=', 'groups.hobby_id')
            ->where('user_hobbies.user_id', $user->id)->select('groups.*');

        $city = $user->location->city;

        return view('groups.index', compact('user', 'groups', 'city'));
    }


    public function group($id){

        if (!$this->secure($id)) return redirect('/404');
        $user = Auth::user();
        $group = $this->group;
        $list = $user->following()->where('allow', 1)->with('following')->get();
        $wall = [
            'new_post_group_id' => $group->id
        ];

        $city = $user->location->city;
        return view('groups.group', compact('user', 'group', 'wall', 'city' ,'list'));
    }



    public function stats($id){

        if (!$this->secure($id)) return redirect('/404');

        $user = Auth::user();

        $group = $this->group;

        $country = $user->location->city->country;
        $city = $user->location->city;

        $all_countries = $group->countAllCountries();

        return view('groups.stats', compact('user', 'group', 'country', 'city', 'all_countries'));
    }

    public function create()
    {
        $user = Auth::user();
        return view('groups.create',compact('user'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        $name = $request->input('name');

        if ($validator->fails()) {
            return redirect('groups/create')
                ->withErrors($validator)
                ->withInput();
        }
        $hobby = new Hobby();
        $hobby->name = $name;
        $hobby->save();
        $hobby_id = $hobby->id;
        $group = new Group();
        $group->hobby_id = $hobby_id;

        if ($group->save()) {
            $request->session()->flash('alert-success', __('message.group_added'));
            return redirect('groups/create');
        } else {
            $request->session()->flash('alert-danger', __('message.group_not_added'));
            return redirect('groups/create');
        }
    }

    public function invite(Request $request){
        $user = Auth::user();
        $response = array();
        $response['code'] = 400;
        //if (!$this->secure($username, true)) return Response::json($response);

        $data = json_decode($request->input('information'), true);


        $validator = Validator::make($data, [
            'invited' => 'required',

        ]);

        if ($validator->fails()) {
            $response['code'] = 400;
            $response['message'] = implode(' ', $validator->errors()->all());
        }else{

            $user = $this->user;
            $user->invited = $data['invited'];
            foreach ($invited as $inv)
            {

            }
            $x = $data['invited'];
            $y = $x[0];
            //$user->birthday = $data['birthday'];
//            $save = $user->save();
//            if ($save){
            $response['code'] = 200;
//
//                if($data['country'] != 0)
//                {
//                    $country = Country::where('nicename', '=', $data['country'])->first();
//                }
//                $oldCity = City::where('name', '=', $data['state'])->first();
//                $city = new City();
//                $locationInfo  = array();
//                //insert city if not exist
//                if(is_null($oldCity))
//                {
//                    $city->country_id = $country->id;
//                    $city->name = $data['state'];
//                    $city->save();
//                    $locationInfo = array('user_id'=>$user->id,'city_id'=>$city->id);
//                }
//                else{
//                    $locationInfo = array('user_id'=>$user->id,'city_id'=>$oldCity->id);
//                }
//
//                $location = UserLocation::find($user->id);
//
//                if(!is_null($location))
//                {
//                    $location->update($locationInfo);
//                }
//                else{
//                    $find_location = new UserLocation();
//                    $find_location->user_id = $user->id;
//                    $find_location->city_id = $data['state'];
//                    $find_location->save();
//                }
//            }
        }

        return Response::json($response);
    }
}
