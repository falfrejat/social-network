<?php

namespace App\Http\Controllers;

use App\Models\AdsCategory;
use App\Models\Ads;
use App\Models\Group;

use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class AdsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
        $setting = Setting::find(1);
        View::share('setting',$setting);
    }
    public function index()
    {
        $user = Auth::user();
        $groups = Group::join('user_hobbies', 'user_hobbies.hobby_id', '=', 'groups.hobby_id')
            ->where('user_hobbies.user_id', $user->id)->select('groups.*');
        $ads_categories = AdsCategory::all();
        //$ads = Ads::orderBy('id', 'desc')->take(10)->get();
        $ads = Ads::orderBy('category_id')->get();

        return view('ads.index', compact('user','groups','ads','ads_categories'));
    }
    public function all()
    {
        $user = Auth::user();
        $groups = Group::join('user_hobbies', 'user_hobbies.hobby_id', '=', 'groups.hobby_id')
            ->where('user_hobbies.user_id', $user->id)->select('groups.*');
        $ads_categories = AdsCategory::all();
        $ads = Ads::orderBy('id', 'desc')->take(20)->get();
        //$ads = Ads::orderBy('category_id')->get();

        return view('ads.all', compact('user','groups','ads','ads_categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        $ads_categories = AdsCategory::all();
        return view('ads.create',compact('user','ads_categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'image.mimes' => trans('validation.mimes'),
            'image.max.file' => trans('validation.max.file'),
        ];
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'content'=> 'required',
            'image' => 'mimes:jpeg,png,jpg,gif,svg|max:2048'
        ],$messages);

        if ($validator->fails()) {
            return redirect('ads/create')
                ->withErrors($validator)
                ->withInput();
        }
        $ad = new Ads();
        if($request->hasFile('image'))
        {
            $file = $request->file('image');
            $file_name = md5(uniqid() . time()) . '.' . $file->getClientOriginalExtension();
            if ($file->move('storage/uploads/ads/', $file_name)) {
                $ad->image = $file_name;
            }
            $ad->title = $request->input('title');
            $ad->text =  $request->input('content');
            $ad->price =  $request->input('price');
            $ad->category_id =  $request->input('category');
            $ad->state =  $request->input('state');
            $user_id = Auth::user()->id;
            $ad->user_id = $user_id;
            $ad->save();
            if ($ad->save()){
                $request->session()->flash('alert-success', __('message.ad_added'));
                return redirect('ads/create');
            }else{
                $request->session()->flash('alert-danger', __('message.ad_not_added'));
                return redirect('ads/create');
            }

        }
        else
        {
            $ad->title = $request->input('title');
            $ad->text =  $request->input('content');
            $ad->price =  $request->input('price');
            $ad->category_id =  $request->input('category');
            $ad->state =  $request->input('state');
            $user_id = Auth::user()->id;
            $ad->user_id = $user_id;
            $ad->save();
            if ($ad->save()){
                $request->session()->flash('alert-success', __('message.ad_added'));
                return redirect('ads/create');
            }else{
                $request->session()->flash('alert-danger', __('message.ad_not_added'));
                return redirect('ads/create');
            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ads  $ads
     * @return \Illuminate\Http\Response
     */
    public function show(Ads $ads)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ads  $ads
     * @return \Illuminate\Http\Response
     */
    public function edit(Ads $ads)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ads  $ads
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ads $ads)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ads  $ads
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ads $ads)
    {
        //
    }

    public function ad($id)
    {
        $ad = Ads::find($id);
        $user = Auth::user();
        return view('ads.ad',compact('ad','user'));
    }
}
