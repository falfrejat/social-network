<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\PostComment;
use App\Models\Reply;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class ReplyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $setting = Setting::find(1);
        View::share('setting',$setting);
    }


    public function reply(Request $request){

        $user = Auth::user();

        $response = array();
        $response['code'] = 400;

        $comment = PostComment::find($request->input('comment_id'));
        $post = Post::find($comment->post_id);
        $text = $request->input('reply');


        if ($comment && !empty($text)){

            $reply = new Reply();
            //$reply->post_id = $post->id;
            $reply->reply_user_id	 = $user->id;
            $reply->reply = $text;
            if ($reply->save()){
                $response['code'] = 200;
                $html = View::make('widgets.post_detail.single_comment', compact('post', 'comment','reply'));
                $response['comment'] = $html->render();
//                $html = View::make('widgets.post_detail.comments_title', compact('post', 'comment'));
//                $response['comments_title'] = $html->render();
            }

        }
        return Response::json($response);
    }
}
