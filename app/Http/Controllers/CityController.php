<?php

namespace App\Http\Controllers;
use App\Models\City;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class CityController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $setting = Setting::find(1);
        View::share('setting',$setting);
    }

    public function getByCountryId($id = 0)
    {
        //$data = City::all();//findOrFail($id);
        $data = City::where('country_id', '=', $id)->get();
        return response()->json($data);
    }
}
