<?php

namespace App\Http\Controllers;

//use App\Http\Requests\StoreJob;
use App\Models\ApplyJob;
use App\Models\Group;

use App\Models\Job;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class JobsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $setting = Setting::find(1);
        View::share('setting',$setting);
    }

    public function index(Request $request)
    {
        $user = Auth::user();
        $groups = Group::join('user_hobbies', 'user_hobbies.hobby_id', '=', 'groups.hobby_id')
            ->where('user_hobbies.user_id', $user->id)->select('groups.*');

        $jobs = Job::orderBy('id', 'desc')->take(10)->get();
        return view('jobs.index', compact('user','groups','jobs'));
    }

    public function create()
    {
        $user = Auth::user();
        return view('jobs.create',compact('user'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'title' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect('jobs/create')
                ->withErrors($validator)
                ->withInput();
        }

        $title = $request->input('title');
        $company = $request->input('company');
        $name = $request->input('name');
        $tags = $request->input('tags');
        $user_id = Auth::user()->id;

        $job = new Job();
        $job->title = $title;
        $job->company = $company;
        $job->text = $name;
        $job->tags = $tags;
        $job->user_id = $user_id;
        $job->save();

        if ($job->save()){
            $request->session()->flash('alert-success', __('message.job_added'));
            return redirect('jobs/create');
        }else{
            $request->session()->flash('alert-danger', __('message.job_not_added'));
            return redirect('jobs/create');
        }
    }

    public function yourjobs()
    {
        $user = Auth::user();
        $jobs = Job::where('user_id', $user->id)->orderBy('id', 'desc')->take(10)->get();
        return view('jobs.yourjobs',compact('jobs','user'));
    }

    public function job($id)
    {
        if(!is_numeric($id))return redirect('/404');
        $user = Auth::user();
        $job = Job::find($id);
        return view('jobs.job',compact('job','user'));
    }
    public function applyjob($id)
    {
        if(!is_numeric($id))return redirect('/404');
        $job = Job::find($id);
        if (!$job) return redirect('/404');
        $user = Auth::user();
        return view('jobs.applyjob',compact('user','id'));
    }

    public function doapplyjob(Request $request)
    {
        $messages = [
            'cv.mimes' => trans('validation.mimes'),
            'cv.max.file' => trans('validation.max.file'),
        ];

        $validator = Validator::make($request->all(), [
            'cv' => 'required|mimes:pdf|max:2048',
                'email' => 'required',
                'phone' => 'required',
                'full_name' => 'required'
        ], $messages);
        $id = $request->input('job_id');
        if ($validator->fails()) {
            return redirect('jobs/applyjob/' . $id)
                ->withErrors($validator)
                ->withInput();
        }else{
            $file = $request->file('cv');
            $file_name = md5(uniqid() . time()) . '.' . $file->getClientOriginalExtension();
            if ($file->move('storage/uploads/applyjob/', $file_name)) {
                $applying_id = Auth::user()->id;

                $apply_job = new ApplyJob();
                $apply_job->cv = $file_name;
                $apply_job->full_name = $request->input('full_name');
                $apply_job->email = $request->input('email');
                $apply_job->phone = $request->input('phone');
                $apply_job->job_id = $id;
                $apply_job->applying_id = $applying_id;
                if ( $apply_job->save()){
                    $request->session()->flash('alert-success', __('message.you_applied_job'));
                    return redirect('jobs/applyjob/' . $id);
                }else{
                    $request->session()->flash('alert-danger', __('message.try_again'));
                    return redirect('jobs/applyjob/' . $id);
                }
            }else{
                $request->session()->flash('alert-danger', __('message.some_thing_wrong'));
                return redirect('jobs/applyjob/' . $id);
            }
        }

    }
    public  function whoapplied($id)
    {
        if(!is_numeric($id))return redirect('/404');

        $applieds = ApplyJob::where('job_id',$id)->get();
        $user = Auth::user();
        //var_dump($applieds);
        return view('jobs.whoapplied',compact('applieds','user'));
    }
    public function downloadCV($apply_job_dd)
    {
        $apply = ApplyJob::find($apply_job_dd);
        $path = public_path('storage/uploads/applyjob/'.$apply->cv);
        return response()->file($path);
    }


}
