<?php

namespace App\Http\Controllers;

use App\ApplyJob;
use Illuminate\Http\Request;

class ApplyJobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ApplyJob  $applyJob
     * @return \Illuminate\Http\Response
     */
    public function show(ApplyJob $applyJob)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ApplyJob  $applyJob
     * @return \Illuminate\Http\Response
     */
    public function edit(ApplyJob $applyJob)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ApplyJob  $applyJob
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ApplyJob $applyJob)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ApplyJob  $applyJob
     * @return \Illuminate\Http\Response
     */
    public function destroy(ApplyJob $applyJob)
    {
        //
    }
}
