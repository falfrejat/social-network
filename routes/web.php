<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Models\Setting;
//use App\Http\Controllers\Auth;
//Route::get('/migrate', function () {
//    $clearcache = Artisan::call('migrate');
//    echo "Migration done<br>";

//});
Route::group(['middleware' => 'guest'], function () {
    Route::get('/', function () {
        $setting = Setting::find(1);
        return view('layouts.guest',compact('setting'));
    });
});
//reset password
Route::get('forget-password', 'Auth\ForgotPasswordController@showForgetPasswordForm')->name('forget.password.get');
Route::post('forget-password', 'Auth\ForgotPasswordController@submitForgetPasswordForm')->name('forget.password.post');
Route::get('reset-password/{token}', 'Auth\ForgotPasswordController@showResetPasswordForm')->name('reset.password.get');
Route::post('reset-password', 'Auth\ForgotPasswordController@submitResetPasswordForm')->name('reset.password.post');

Auth::routes();
Route::get('register/verify/{confirmationCode}', [
    'as' => 'confirmation_path',
    'uses' => 'Auth\RegisterController@confirm'
]);


//Admin Routes
Route::group(['namespace' => 'Admin'],function (){

    Route::get('admin/users','AdminUsersController@index')->name('admin.users');
    Route::get('admin/users/add','AdminUsersController@create')->name('users.add');
    Route::get('admin/users/{id}','AdminUsersController@edit')->name('users.edit');
    Route::delete('admin/users/destroy','AdminUsersController@destroy')/*->name('admin.users.destroy')*/;
    Route::get('admin/users/deactivate/{id}','AdminUsersController@deactivate');
    Route::get('admin/users/activate/{id}','AdminUsersController@activate');

    Route::get('admin/home','HomeController@index')->name('admin.home');
    Route::get('admin-login','Auth\LoginController@showLoginForm')->name('admin.login');
    Route::post('admin-login','Auth\LoginController@login');
    Route::get('admin-logout','Auth\LoginController@logout')->name('admin.logout');

    //cities
    Route::get('admin/cities','AdminCityController@index')->name('admin.cities');
    Route::get('admin/cities/add','AdminCityController@create')->name('city.add');
    Route::post('/admin/cities/store', 'AdminCityController@store');
    Route::get('admin/cities/edit/{id}','AdminCityController@edit');//->name('countries.edit');
    Route::post('admin/cities/update/{id}','AdminCityController@update');
    Route::delete('admin/cities/destroy/{id}',  'AdminCityController@destroy')->name('admin.cities.destroy');

    Route::get('admin/countries','AdminCountriesController@index')->name('admin.countries');
    Route::get('admin/countries/add','AdminCountriesController@create')->name('countries.add');
    Route::post('/admin/countries/store', 'AdminCountriesController@store');
    Route::get('admin/countries/edit/{id}','AdminCountriesController@edit')->name('countries.edit');
    Route::post('admin/countries/update/{id}','AdminCountriesController@update');
    Route::delete('admin/countries/destroy/{id}',  'AdminCountriesController@destroy')->name('admin.countries.destroy');

    //setting
    Route::get('admin/setting','AdminSettingController@index')->name('admin.settings');
    Route::post('admin/setting/update','AdminSettingController@update');

    //posts
    Route::get('admin/posts','AdminPostController@index');
    Route::get('admin/posts/hide/{id}',  'AdminPostController@hide');
    Route::get('admin/posts/unhide/{id}',  'AdminPostController@unhide');
    Route::delete('admin/posts/destroy/{id}',  'AdminPostController@destroy')->name('admin.posts.destroy');

    //comments
    Route::get('admin/comments',  'AdminCommentsController@index');
    Route::get('admin/comments/edit/{id}','AdminCommentsController@edit')->name('comments.edit');
    Route::post('admin/comments/update/{id}','AdminCommentsController@update');
    Route::delete('admin/comments/destroy/{id}',  'AdminCommentsController@destroy')->name('admin.comments.destroy');

    //Jobs
    Route::get('admin/jobs','AdminJobsController@index')->name('admin.jobs');
    Route::get('admin/jobs/edit/{id}','AdminJobsController@edit')->name('jobs.edit');
    Route::delete('admin/users/destroy/{id}',  'AdminJobsController@destroy')->name('admin.jobs.destroy');

    //Library
    Route::get('admin/library','AdminLibraryController@index')->name('admin.library');
    Route::get('admin/library/{id}/downloadbook','AdminLibraryController@downloadBook');
    //logger
    Route::get('admin/logger','AdminLoggerController@index')->name('admin.logger');
});



Route::get('/home', 'HomeController@index')->name('home');
Route::get('/settings', 'SettingsController@index');
Route::post('/settings', array(
    'as' => 'settings',
    'uses' => 'SettingsController@update'
));

//page
Route::get('pages', 'PageController@index');
Route::get('pages/create','PageController@create');
Route::post('pages/create','PageController@store');
Route::get('page/{id}','PageController@page');

/*Route::get('/group/{id}', 'GroupController@group');
Route::get('/group/{id}/stats', 'GroupController@stats');

Route::post('groups/create','GroupController@store');*/
// Posts
Route::get('/posts/list', 'PostsController@fetch');
Route::post('/posts/new', 'PostsController@create');
Route::post('/posts/delete', 'PostsController@delete');
Route::post('/posts/like', 'PostsController@like');
Route::post('/posts/likes', 'PostsController@likes');
Route::post('/posts/comment', 'PostsController@comment');
Route::post('/posts/comments/delete', 'PostsController@deleteComment');
Route::post('/posts/comments/hide', 'PostsController@hideComment');
Route::get('/test', 'PostsController@test');

Route::get('/post/{id}', 'PostsController@single');
Route::get('/post/edit/{id}', 'PostsController@edit');
Route::post('/posts/update', 'PostsController@update');
Route::get('post/{link}/downloadbook','PostsController@downloadDoc');
//post/'.$image->getURLDoc().'/downloadbook
//reply comment
Route::post('/comment/reply', 'ReplyController@reply');
//save posts
Route::post('/posts/save', 'PostsController@savePost');
Route::get('/saved-posts', 'SavePostController@index');

//library
Route::get('library','LibraryController@index');
Route::get('library/{id}/downloadbook','LibraryController@downloadBook');
Route::get('library/create','LibraryController@create');
Route::post('library/store','LibraryController@store');
Route::get('library/yourbooks','LibraryController@yourbooks');
Route::get('library/book/{id}','LibraryController@book');

// Search
Route::get('/search', 'HomeController@search');

// Groups
Route::get('/groups', 'GroupController@index');
Route::get('/group/{id}', 'GroupController@group');
Route::get('/group/{id}/stats', 'GroupController@stats');
Route::get('groups/create','GroupController@create');
Route::post('groups/create','GroupController@store');
Route::post('group/invite','GroupController@invite');


//jobs
Route::get('/jobs','JobsController@index');
Route::get('/jobs/job/{id}', 'JobsController@job');
Route::get('jobs/create','JobsController@create');
Route::post('jobs/create','JobsController@store');
Route::get('jobs/yourjobs','JobsController@yourjobs');
Route::get('jobs/applyjob/{id}','JobsController@applyjob');
Route::post('jobs/doapplyjob','JobsController@doapplyjob');
Route::get('jobs/who-applied/{id}','JobsController@whoapplied');
Route::get('job/{id}/downloadcv','JobsController@downloadCV');

//work experience
Route::post('workexp/{id}/update','WorkExpController@update');
Route::get('workexp/{id}/downloadcv','WorkExpController@downloadCV');

//ads
Route::get('/ads','AdsController@index');
Route::get('ad/{id}','AdsController@ad');
Route::get('ads/create','AdsController@create');
Route::post('ads/create','AdsController@store');
Route::get('ads/all','AdsController@all');
//Route::get('jobs/yourads','AdsController@yourads');

// Follow
Route::post('/follow', 'FollowController@follow');
Route::get('/followers/pending', 'FollowController@pending');
Route::post('/follower/request', 'FollowController@followerRequest');
Route::post('/follower/denied', 'FollowController@followDenied');

// Relatives
Route::get('/relatives/pending', 'RelativesController@pending');
Route::post('/relative/delete', 'RelativesController@delete');
Route::post('/relative/request', 'RelativesController@relativeRequest');

// Nearby
Route::get('/nearby', 'NearbyController@index');

// Messages
Route::get('/direct-messages', 'MessagesController@index');
Route::get('/direct-messages/show/{id}', 'MessagesController@index');
Route::post('/direct-messages/chat', 'MessagesController@chat');
Route::post('/direct-messages/send', 'MessagesController@send');
Route::post('/direct-messages/new-messages', 'MessagesController@newMessages');
Route::post('/direct-messages/people-list', 'MessagesController@peopleList');
Route::post('/direct-messages/delete-chat', 'MessagesController@deleteChat');
Route::post('/direct-messages/delete-message', 'MessagesController@deleteMessage');
Route::post('/direct-messages/notifications', 'MessagesController@notifications');

// Find Location
Route::get('/find-my-location', 'FindLocationController@index');
Route::get('/save-my-location', 'FindLocationController@save');
Route::get('/save-my-location2', 'FindLocationController@save2');

// Profile
Route::get('/{username}', 'ProfileController@index');
Route::post('/{username}/upload/profile-photo', 'ProfileController@uploadProfilePhoto');
Route::post('/{username}/upload/cover', 'ProfileController@uploadCover');
Route::post('/{username}/save/information', 'ProfileController@saveInformation');
Route::get('/{username}/following', 'ProfileController@following');
Route::get('/{username}/followers', 'ProfileController@followers');
Route::post('/{username}/save/hobbies', 'ProfileController@saveHobbies');
Route::post('/{username}/save/relationship', 'ProfileController@saveRelationship');

//lang
Route::get('lang/home', 'LangController@index');
Route::get('lang/change', 'LangController@change')->name('changeLang');

//city
Route::get('city/getCountryById/{id}', 'CityController@getByCountryId')->name('city.getByCountryId');

Route::get('getFollowers', 'FollowController@getFollowers')->name('getFollowers');





